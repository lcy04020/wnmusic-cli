package com.wnmusic.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GetRandomList {

    public static List<Integer> getRandomList(int count,int minNum ,int maxNum){
        List<Integer> list = new ArrayList<>();
        Random random = new Random();
        for(int i = 0;i<count;i++){
            int j = random.nextInt(maxNum)+minNum;
            if(list.contains(j)){
                i--;
                continue;
            }else{
                list.add(j);
            }
        }
        return list;
    }

//    public static void main(String[] args) {
//        List<Integer> randomList = GetRandomList.getRandomList(48, 1, 80);
//        System.out.println(randomList);
//    }
}
