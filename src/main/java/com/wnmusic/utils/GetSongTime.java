package com.wnmusic.utils;

import javazoom.jl.decoder.Bitstream;
import javazoom.jl.decoder.BitstreamException;
import javazoom.jl.decoder.Header;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class GetSongTime {
    public static int getSongTime(String toUrl) {

        int songTime = 0;
        try {
            long startTime=System.currentTimeMillis();   //获取开始时间
            URL urlfile = new URL(toUrl);
            //File file = new File("C:\\music\\test2.mp3");
            //URL urlfile = file.toURI().toURL();
            URLConnection con = urlfile.openConnection();
            int b = con.getContentLength();// 得到音乐文件的总长度
            BufferedInputStream bis = new BufferedInputStream(con.getInputStream());
            Bitstream bt = new Bitstream(bis);
            Header h = bt.readFrame();
            int time = (int) h.total_ms(b);
            songTime = time / 1000;
            long endTime1=System.currentTimeMillis(); //获取结束时间
            System.out.println("所需时间： "+(endTime1-startTime)+"ms");
            System.out.println("歌曲时长： "+songTime+"s");
        }catch (Exception e ){
            System.out.println(e.getMessage());
        }
        return songTime;
        //        BufferedInputStream bis = null;
//        Bitstream bitstream = null;
//        int timeLong = 0;
//        try {
//            URL url = new URL(toUrl);
//            HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
//            urlConn.connect();
//            //此url响应头中包含"Accept-Length"为字节数大小
//            String headerField = urlConn.getHeaderField("Content-Length");
//            bis = new BufferedInputStream(urlConn.getInputStream());
//            bitstream = new Bitstream(bis);
//            Header header = bitstream.readFrame();
//            int bitrate = header.bitrate();
//            //根据文件大小计算 字节数*8/码率/1000（毫秒值转秒）
//            timeLong = Integer.parseInt(headerField) * 8 / bitrate;
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (bis != null) {
//                try {
//                    bis.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            if (bitstream != null) {
//                try {
//                    bitstream.close();
//                } catch (BitstreamException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        return timeLong;
    }

    public static String secToMin(int sec){
        String m = String.valueOf(sec/60).substring(0,1);
        String s = String.valueOf(sec%60);
        return m+":"+s;
    }


    public static void main(String[] args) {
        String s = secToMin(256);
        System.out.println(s);
    }
}
