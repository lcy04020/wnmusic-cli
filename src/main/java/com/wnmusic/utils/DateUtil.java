package com.wnmusic.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class DateUtil {

    /**
     *
     * @param pattern : 需要的时间格式
     * @return 得到时间
     */
    public static String getTime(String pattern){
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        return format.format(date);
    }

    /**
     *
     * @return 得到毫秒数的时间
     */
    public static long getLongTime() {
        Date date = new Date();
        return date.getTime();
    }

    /**
     *
     * @param time  需要转换的的时间字符串
     * @param pattern   转换格式
     * @return  转换后的时间
     */
    public static Date stringToDate(String time,String pattern){
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            return format.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param date  需要转换的时间对象
     * @param pattern   需要的格式
     * @return  转换后的时间字符串
     */
    public static String dateToString(Date date,String pattern){
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        return format.format(date);
    }


}
