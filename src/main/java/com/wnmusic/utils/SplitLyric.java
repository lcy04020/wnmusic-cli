package com.wnmusic.utils;

import com.wnmusic.service.SongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SplitLyric {

    SongService songService;

    @Autowired
    public void setSongService(SongService songService) {
        this.songService = songService;
    }

    public static String[][] getLyricArrays(String lyric){
        String[] split = lyric.split("\\[");
        String[][] songLyric = new String[100][2];
        for (int i = 1;i<split.length-1;i++) {
            String[] split1 = split[i].split("\\]");
            songLyric[i-1] = split1;
        }
        return songLyric;
    }


}
