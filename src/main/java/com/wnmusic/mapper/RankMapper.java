package com.wnmusic.mapper;

import com.wnmusic.queryvo.SongAlbumRankQueryVo;
import com.wnmusic.queryvo.SongListRankQueryVo;
import com.wnmusic.queryvo.SongRankQueryVo;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author LCY
* @description 针对表【song_rank】的数据库操作Mapper
* @createDate 2022-09-02 09:56:51
* @Entity com.wnmusic.entity.SongRank
*/
@Repository
public interface RankMapper {

    List<SongRankQueryVo> selectAllSongRankAvg();

    List<SongRankQueryVo> selectAllSongRankAvgAndTime();

    List<SongListRankQueryVo> selectAllSongListRankAvg();

    List<SongListRankQueryVo> selectAllSongListRankAvgByStyle(String style);

    List<SongAlbumRankQueryVo> selectAllSongAlbumRankAvg();

}
