package com.wnmusic.mapper;

import com.wnmusic.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
* @author syy
* @description 针对表【user(前端用户)】的数据库操作Mapper
* @createDate 2022-09-02 15:15:06
* @Entity com.wnmusic.entity.User
*/
@Repository
public interface UserMapper {



    int deleteByPrimaryKey(Integer id);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(User record);

    /**
     * 添加用户
     * @param record
     * @return
     */
    int insert(User record);

    /**
     * 根据用户名和密码查找用户
     *
     * @param userName 用户名
     * @return 返回一个User对象
     */
    User selectUserByNameAndPassword(@Param("userName") String userName);

    /**
     * 修改用户信息
     * @param user 用户对象
     * @return     true代表有此用户，false代表没找到值为null
     */
    Boolean updateUser(User user);
}
