package com.wnmusic.mapper;

import com.wnmusic.entity.Song;
import com.wnmusic.entity.SongList;
import com.wnmusic.queryvo.FindSongQueryVo;
import com.wnmusic.queryvo.SongDetailVo;
import com.wnmusic.queryvo.SongPlayerQueryVo;
import org.springframework.stereotype.Repository;

import java.lang.annotation.Repeatable;
import java.util.List;

/**
* @author LCY
* @description 针对表【song(歌曲)】的数据库操作Mapper
* @createDate 2022-09-01 19:09:53
* @Entity com.wnmusic.entity.Song
*/
@Repository
public interface SongMapper {

    List<Song> selectAll();

    Song selectById(Integer id);

    SongPlayerQueryVo selectSongPlayerById(Integer id);

    List<Song> selectFifteen();

    List<Song> selectOldFifteen();

    List<Song> selectClaFifteen();

    List<Song> selectFifteenByRank();

    List<Song> selectBySingerName(String singerName);

    SongDetailVo selectSongDetailVoByKey(Integer id);

    List<Song> randomSelect(List<Integer> list);

    List<FindSongQueryVo> fuzzySelect(String songName);

    /**
     * 根据歌曲ids 查询歌曲列表
     * @param ids songIds list
     * @return
     */
    List<SongDetailVo> selectSongListDetailVoByKey(List<Integer> ids);

    List<SongPlayerQueryVo> selectByList(List<Integer> songIds);
}
