package com.wnmusic.mapper;

import com.wnmusic.entity.SongAlbum;
import com.wnmusic.entity.SongList;
import com.wnmusic.queryvo.FindSongAlbumQueryVo;
import com.wnmusic.queryvo.SongAlbumPlayerQueryVo;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author LCY
* @description 针对表【song_album】的数据库操作Mapper
* @createDate 2022-09-01 19:58:05
* @Entity com.wnmusic.entity.SongAlbum
*/
@Repository
public interface SongAlbumMapper {

    List<SongAlbum> selectAll();

    List<SongAlbum> selectFifteen();

    List<SongAlbum> randomSelect(List<Integer> list);

    List<SongAlbum> selectByStyle(String style);

    SongAlbumPlayerQueryVo selectByIdToPlayer(Integer id);

    List<Integer> selectSongIdByAlbumId(Integer albumId);

    List<String> selectSingerNameByAlbumId(Integer albumId);

    SongAlbum selectSongAlbumByPrimaryKey(Integer albumId);

    List<Integer> selectSingerIdByAlbumId(Integer albumId);

    List<String> selectAllDistinctStyle();

    List<FindSongAlbumQueryVo> fuzzySelect(String songName);

    List<Integer> selectSongIdsById(Integer id);

    SongAlbum selectById(Integer id);
}
