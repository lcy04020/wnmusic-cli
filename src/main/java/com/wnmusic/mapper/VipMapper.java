package com.wnmusic.mapper;

import com.wnmusic.entity.Vip;
import io.swagger.models.auth.In;
import org.springframework.stereotype.Repository;

/**
* @author LCY
* @description 针对表【vip】的数据库操作Mapper
* @createDate 2022-09-09 11:02:05
* @Entity com.wnmusic.entity.Vip
*/
@Repository
public interface VipMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Vip record);

    Vip selectByPrimaryKey(Integer id);

    Integer selectStatusByUserId(Integer userId);

    // 查寻vip表有没有这个vip
    Vip selectVipByUserId(Integer userId);

    // 如果有vip 则修改 没有则新增
    int updateVipByUserId(Vip vip);

}
