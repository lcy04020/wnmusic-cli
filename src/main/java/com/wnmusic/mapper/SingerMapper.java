package com.wnmusic.mapper;

import com.wnmusic.entity.Singer;
import com.wnmusic.entity.SongList;
import com.wnmusic.queryvo.SingerAllMsgVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 田红卫
 * @description 针对表【singer(歌手)】的数据库操作Mapper
 * @createDate 2022-09-02 18:51:57
 * @Entity com.wnmusic.entity.mapper.Singer
 */
@Repository
public interface SingerMapper {

    int deleteByPrimaryKey(Long id);

    int insert(Singer record);

    int insertSelective(Singer record);

    Singer selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Singer record);

    int updateByPrimaryKey(Singer record);

    List<Singer> randomSelect(List<Integer> list);

    /**
     *  所有的歌手的条件查询
     * @param singerAddress 歌手地址
     * @param singerGender 歌手性别
     * @return 返回一个list集合
     */
    List<Singer> selectSingerAllByCondition(
            @Param("singerAddress")String singerAddress,
            @Param("singerGender")String singerGender);

    /**
     *  内地的歌手的条件查询
     * @param singerGender 歌手性别
     * @return 返回一个list集合
     */
    List<Singer> selectSingerInlandByCondition(
            @Param("singerGender")String singerGender);
    /**
     *  其他类型歌手的通用条件查询
     * @param singerGender 歌手性别
     * @param singerAddresses 歌手地址
     * @return 返回一个list集合
     */
    List<Singer> selectSingerOtherByCondition(
            @Param("singerGender")String singerGender,
            @Param("singerAddresses")String...singerAddresses);

    /**
     * 封装了一个vo 记录歌手的所有信息
     * @param singerId 歌手的id
     * @return 返回一个封装的vo
     */
    List<SingerAllMsgVo> selectAllSingerMsg(@Param("singerId") Integer singerId);

    /**
     * 查找歌曲数量
     * @param singerId 歌手的id
     * @return 返回个数
     */
    Integer singerCountSong(@Param("singerId")Integer singerId);
    /**
     * 查找专辑数量
     * @param singerId 歌手的id
     * @return 返回个数
     */
    Integer singerCountSongAlbum(@Param("singerId")Integer singerId);

    int selectSingerByName(String singerName);
}
