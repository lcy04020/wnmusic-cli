package com.wnmusic.mapper;

import com.wnmusic.entity.VipSong;
import org.springframework.stereotype.Repository;

/**
* @author LCY
* @description 针对表【vip_song】的数据库操作Mapper
* @createDate 2022-09-09 11:02:23
* @Entity com.wnmusic.entity.VipSong
*/
@Repository
public interface VipSongMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(VipSong record);

    VipSong selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(VipSong record);

    Integer selectStatusBySongId(Integer songId);
}
