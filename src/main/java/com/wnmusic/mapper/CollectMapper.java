package com.wnmusic.mapper;


import com.wnmusic.queryvo.CollectQueryVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author 张
* @description 针对表【collect(收藏)】的数据库操作Mapper
* @createDate 2022-09-07 12:04:40
* @Entity com.wnmusic.entity.Collect
*/
public interface CollectMapper {


    List<CollectQueryVo> findUserByCollect(@Param("userName") String userName);

    List<CollectQueryVo> findSongAlbumByCollect(@Param("userName") String userName);

    List<CollectQueryVo> findSongListByCollect(@Param("userName") String userName);


    // CollectQueryVo findUserByCollect(@Param("userName") String userName);

}




