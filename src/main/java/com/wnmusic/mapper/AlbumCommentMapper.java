package com.wnmusic.mapper;

import com.wnmusic.entity.AlbumComment;
import com.wnmusic.queryvo.AlbumCommentQueryVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
* @author 田红卫
* @description 针对表【album_comment(评论)】的数据库操作Mapper
* @createDate 2022-09-11 23:33:41
* @Entity com.wnmusic.entity.AlbumComment
*/

@Component
public interface AlbumCommentMapper {

    int deleteByPrimaryKey(Long id);

    int insert(AlbumComment record);

    AlbumComment selectByPrimaryKey(Long id);

    int updateByPrimaryKey(AlbumComment record);

    List<AlbumCommentQueryVo> selectAllCommentByAlbumId(Integer albumId);

    int selectCountUserId(@Param("userId") Integer userId, @Param("commentId") Integer commentId);

    int insertUp(@Param("userId") Integer userId, @Param("commentId") Integer commentId);

    int deleteUp(@Param("userId") Integer userId, @Param("commentId") Integer commentId);

    int updateUpByPrimaryKey(@Param("commentId") Integer commentId, @Param("i") int i);

    List<AlbumCommentQueryVo> selectCommentReplyByParentId(Integer parentId);

    int updateReply(Integer id);

    List<AlbumCommentQueryVo> selectAllCommentByAlbumIdOrderByUp(Integer albumId);

}
