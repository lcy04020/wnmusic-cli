package com.wnmusic.mapper;
import com.wnmusic.entity.SongList;
import com.wnmusic.queryvo.SongListPlayerQueryVo;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author LCY
* @description 针对表【song_list(歌单)】的数据库操作Mapper
* @createDate 2022-09-01 15:52:50
* @Entity com.wnmusic.entity.SongList
*/
@Repository
public interface SongListMapper {

    List<SongList> selectAll();

    List<SongList> selectFifteen();

    List<SongList> selectByStyle(String style);

    int deleteByPrimaryKey(Long id);

    int insert(SongList record);

    int insertSelective(SongList record);

    SongList selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SongList record);

    int updateByPrimaryKey(SongList record);

    List<SongList> selectAllSongList();

    List<SongList> randomSelect(List<Integer> list);

    SongListPlayerQueryVo selectByIdToPlayer(Integer id);

    List<Integer> selectSongIdByListId(Integer listId);

    SongList selectSongListByPrimaryKey(Integer listId);

    List<String> selectAllDistinctStyle();

    List<SongList> fuzzySelect(String songName);

    List<Integer> selectSongIdsById(Integer id);

    SongList selectById(Integer id);
}
