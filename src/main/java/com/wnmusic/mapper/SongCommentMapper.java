package com.wnmusic.mapper;

import com.wnmusic.entity.SongComment;
import com.wnmusic.queryvo.SongCommentQueryVo;
import lombok.experimental.PackagePrivate;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author 田红卫
* @description 针对表【song_comment(评论)】的数据库操作Mapper
* @createDate 2022-09-07 12:15:14
* @Entity com.wnmusic.entity.SongComment
*/

@Repository
public interface SongCommentMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SongComment record);

    SongComment selectByPrimaryKey(Long id);

    int updateByPrimaryKey(SongComment record);

    List<SongCommentQueryVo> selectAllCommentBySongId(Integer songId);

    int selectCountUserId(@Param("userId") Integer userId, @Param("commentId") Integer commentId);

    int insertUp(@Param("userId") Integer userId, @Param("commentId") Integer commentId);

    int deleteUp(@Param("userId") Integer userId, @Param("commentId") Integer commentId);

    int updateUpByPrimaryKey(@Param("commentId") Integer commentId, @Param("i") int i);

    List<SongCommentQueryVo> selectCommentReplyByParentId(Integer parentId);

    int updateReply(Integer id);

    List<SongCommentQueryVo> selectAllCommentBySongIdOrderByUp(Integer songId);
}
