package com.wnmusic.mapper;

import com.wnmusic.entity.ListComment;
import com.wnmusic.queryvo.ListCommentQueryVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
* @author 田红卫
* @description 针对表【list_comment(评论)】的数据库操作Mapper
* @createDate 2022-09-11 23:33:33
* @Entity com.wnmusic.entity.ListComment
*/

@Component
public interface ListCommentMapper {

    int deleteByPrimaryKey(Long id);

    int insert(ListComment record);

    int insertSelective(ListComment record);

    ListComment selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ListComment record);

    int updateByPrimaryKey(ListComment record);

    List<ListCommentQueryVo> selectAllCommentByListId(Integer listId);

    int selectCountUserId(@Param("userId") Integer userId, @Param("commentId") Integer commentId);

    int insertUp(@Param("userId") Integer userId, @Param("commentId") Integer commentId);

    int deleteUp(@Param("userId") Integer userId, @Param("commentId") Integer commentId);

    int updateUpByPrimaryKey(@Param("commentId") Integer commentId, @Param("i") int i);

    List<ListCommentQueryVo> selectCommentReplyByParentId(Integer parentId);

    int updateReply(Integer id);

    List<ListCommentQueryVo> selectAllCommentByListIdOrderByUp(Integer listId);

}
