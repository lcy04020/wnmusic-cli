package com.wnmusic.service;

import com.wnmusic.entity.SongList;
import com.wnmusic.queryvo.SongListPlayerQueryVo;
import com.wnmusic.queryvo.ListDetailVo;

import java.util.List;

public interface SongListService {
    List<SongList> findAll();

    List<SongList> findFifteen();

    List<SongList> findByStyle(String style);

    int deleteByPrimaryKey(Long id);

    int insert(SongList record);

    int insertSelective(SongList record);

    SongList selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SongList record);

    int updateByPrimaryKey(SongList record);

    List<SongList> queryAllSongList();

    List<SongList> randomFind();

    SongListPlayerQueryVo findByIdToPlayer(Integer id);

    ListDetailVo findListDetailVoByKey(Integer listId);

    List<String> findAllDistinctStyle();

    List<SongList> fuzzyFind(String songName);

    List<Integer> findSongIdsById(Integer id);

    SongList findById(Integer id);
}
