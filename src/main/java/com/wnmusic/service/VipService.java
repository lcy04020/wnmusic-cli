package com.wnmusic.service;

import com.wnmusic.entity.Vip;

/**
 * @author lcy
 * @date 2022/9/9 11:06
 */
public interface VipService {

    int removeByPrimaryKey(Integer id);

    int addVip(Vip record);

    Vip findByPrimaryKey(Integer id);

    Integer findStatusByUserId(Integer userId);

    Vip findVipByUserId(Integer userId);

    int modifyVipByUserId(Vip vip);
}
