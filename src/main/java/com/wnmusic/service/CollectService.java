package com.wnmusic.service;


import com.wnmusic.queryvo.CollectQueryVo;

import java.util.List;

/**
* @author 张
* @description 针对表【collect(收藏)】的数据库操作Service
* @createDate 2022-09-07 12:04:40
*/
public interface CollectService {


    List<CollectQueryVo> selectAllCollect(String userName);

    List<CollectQueryVo> selectAllSongAlbum(String userName);

    List<CollectQueryVo> selectAllSongList(String userName);
}
