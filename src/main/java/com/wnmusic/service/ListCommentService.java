package com.wnmusic.service;

import com.wnmusic.entity.ListComment;
import com.wnmusic.entity.ListComment;
import com.wnmusic.queryvo.ListCommentQueryVo;

import java.util.List;

public interface ListCommentService {
    int removeByPrimaryKey(Long id);

    int add(ListComment record);

    ListComment findByPrimaryKey(Long id);

    int modifyByPrimaryKey(ListComment record);

    List<ListCommentQueryVo> findAllCommentByListId(Integer listId, String by);

    boolean upOrNot(Integer userId, Integer commentId);

    boolean upAndCancel(Integer userId, Integer commentId);

    List<ListCommentQueryVo> findListCommentReplyByParentId(Integer parentId);

    int addChildComment(ListComment comment);
}
