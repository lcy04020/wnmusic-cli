package com.wnmusic.service;

import com.wnmusic.entity.Song;
import com.wnmusic.queryvo.FindSongQueryVo;
import com.wnmusic.queryvo.SongDetailVo;
import com.wnmusic.queryvo.SongPlayerQueryVo;

import java.util.List;

public interface SongService {
    List<Song> findAll();

    Song findById(Integer id);

    SongPlayerQueryVo findSongPlayerById(Integer id);

    List<Song> findFifteen();

    List<Song> findOldFifteen();

    List<Song> findClaFifteen();

    List<Song> findFifteenByRank();

    List<Song> findBySingerName(String singerName);

    SongDetailVo findSongDetailVoById(Integer id);

    List<FindSongQueryVo> fuzzyFind(String songName);

    List<Song> randomFind();

    List<SongPlayerQueryVo> findByListId(List<Integer> songIds);
}
