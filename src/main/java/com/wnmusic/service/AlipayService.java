package com.wnmusic.service;


import java.math.BigDecimal;


/**
 * @author syy
 * @date 2022/09/11
 **/
public interface AlipayService {


    /**
     * 生成支付表单
     *
     * @param subject
     * @param money
     * @return
     * @throws Exception
     */
    String toPay(String subject, BigDecimal money) throws Exception;

    /**
     * 通过时间生成外部订单号 out_trade_no
     *
     * @return
     */
    String generateTradeNo();

    /**
     * 查询交易状态
     *
     * @param outTradeNo 生成的外部订单号 out_trade_no
     * @return
     * @throws Exception
     */
    Object queryTradeStatus(String outTradeNo) throws Exception;


}
