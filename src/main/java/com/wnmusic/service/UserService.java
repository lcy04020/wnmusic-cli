package com.wnmusic.service;

import com.wnmusic.entity.User;

/**
 * @author syy
 * @date 2022/09/02
 **/
public interface UserService {

    /**
     * 用户注册
     * @param user
     * @return
     */
    boolean userRegister(User user);

    /**
     * 根据用户名和密码查找用户
     *
     * @param userName 用户名
     * @param userPassword 密码
     * @return  1表示找到用户； 0表示密码错误； -1表示用户名错误
     */
    int userLogin(String userName,String userPassword);

    /**
     * 查询用户的所有信息
     * @param userName
     * @return
     */
    User findUserAllMsg(String userName);


    /**
     * 检验数据库是否重名
     * @param username
     * @return
     */
    boolean findUserByName(String username);

    /**
     * token
     *
     * @param userId
     * @return
     */
    User findUserByPrimaryKey(Integer userId);

    User findUserByUserName(String username);

    Boolean modifyUser(User user);
}
