package com.wnmusic.service;

import com.wnmusic.queryvo.SongAlbumRankQueryVo;
import com.wnmusic.queryvo.SongListRankQueryVo;
import com.wnmusic.queryvo.SongRankQueryVo;

import java.util.List;

public interface RankService {
    List<SongRankQueryVo> findAllSongRankAvg();

    List<SongRankQueryVo> findAllSongRankAvgAndTime();

    List<SongListRankQueryVo> findAllSongListRankAvg();

    List<SongListRankQueryVo> findAllSongListRankAvgByStyle(String style);

    List<SongAlbumRankQueryVo> findAllSongAlbumRankAvg();
}
