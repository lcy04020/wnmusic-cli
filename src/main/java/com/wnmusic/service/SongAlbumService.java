package com.wnmusic.service;

import com.wnmusic.entity.SongAlbum;
import com.wnmusic.entity.SongList;
import com.wnmusic.queryvo.FindSongAlbumQueryVo;
import com.wnmusic.queryvo.SongAlbumPlayerQueryVo;
import com.wnmusic.queryvo.AlbumDetailVo;

import java.util.List;

public interface SongAlbumService {
    List<SongAlbum> findAll();

    List<SongAlbum> findFifteen();

    List<SongAlbum> randomFind();

    List<SongAlbum> findByStyle(String style);

    SongAlbumPlayerQueryVo findByIdToPlayer(Integer id);

    AlbumDetailVo findAlbumDetailVoByKey(Integer albumId);

    List<String> findAllDistinctStyle();

    List<FindSongAlbumQueryVo> fuzzyFind(String songName);

    List<Integer> findSongIdsById(Integer id);

    SongAlbum findById(Integer id);
}
