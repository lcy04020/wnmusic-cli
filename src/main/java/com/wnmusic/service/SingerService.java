package com.wnmusic.service;

import com.wnmusic.entity.Singer;
import com.wnmusic.queryvo.SingerAllMsgVo;

import java.util.List;

/**
 * @author syy
 * @date 2022/09/07
 **/
public interface SingerService {

    /**
     * 条件查询
     * @param singerGender 歌手性别
     * @return 返回一个list集合
     */
    List<Singer> findSingerAllByCondition(
            String singerAddress,
            String singerGender);

    /**
     *  内地的歌手的条件查询
     * @param singerGender 歌手性别
     * @return 返回一个list集合
     */
    List<Singer> findSingerInlandByCondition(
            String singerGender);

    /**
     *  其他类型歌手的通用条件查询
     * @param singerGender 歌手性别
     * @param singerAddresses 歌手地址
     * @return 返回一个list集合
     */
    List<Singer> findSingerOtherByCondition(
            String singerGender,
            String...singerAddresses);

    /**
     * 封装了一个vo 记录歌手的所有信息
     * @param singerId 歌手的id
     * @return 返回一个封装的vo
     */
    List<SingerAllMsgVo> selectAllSingerMsg( Integer singerId);

    /**
     * 查找歌曲数量
     * @param singerId 歌手的id
     * @return 返回个数
     */
    Integer singerCountSong(Integer singerId);
    /**
     * 查找专辑数量
     * @param singerId 歌手的id
     * @return 返回个数
     */
    Integer singerCountSongAlbum(Integer singerId);

    int findSingerByName(String singerName);
}
