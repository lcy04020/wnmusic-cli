package com.wnmusic.service;

import com.wnmusic.entity.AlbumComment;
import com.wnmusic.queryvo.AlbumCommentQueryVo;
import java.util.List;

public interface AlbumCommentService {
    int removeByPrimaryKey(Long id);

    int add(AlbumComment record);

    AlbumComment findByPrimaryKey(Long id);

    int modifyByPrimaryKey(AlbumComment record);

    List<AlbumCommentQueryVo> findAllCommentByAlbumId(Integer albumId, String by);

    boolean upOrNot(Integer userId, Integer commentId);

    boolean upAndCancel(Integer userId, Integer commentId);

    List<AlbumCommentQueryVo> findAlbumCommentReplyByParentId(Integer parentId);

    int addChildComment(AlbumComment comment);
}
