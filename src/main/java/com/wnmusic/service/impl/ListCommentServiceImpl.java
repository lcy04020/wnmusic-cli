package com.wnmusic.service.impl;

import com.wnmusic.entity.ListComment;
import com.wnmusic.entity.ListComment;
import com.wnmusic.mapper.ListCommentMapper;
import com.wnmusic.mapper.ListCommentMapper;
import com.wnmusic.queryvo.ListCommentQueryVo;
import com.wnmusic.service.ListCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ListCommentServiceImpl implements ListCommentService {
    private ListCommentMapper listCommentMapper;

    @Autowired
    public void setListCommentMapper(ListCommentMapper listCommentMapper) {
        this.listCommentMapper = listCommentMapper;
    }

    @Override
    public int removeByPrimaryKey(Long id) {
        return listCommentMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int add(ListComment record) {
        return listCommentMapper.insert(record);
    }


    @Override
    public ListComment findByPrimaryKey(Long id) {
        return listCommentMapper.selectByPrimaryKey(id);
    }


    @Override
    public int modifyByPrimaryKey(ListComment record) {
        return listCommentMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<ListCommentQueryVo> findAllCommentByListId(Integer listId, String by) {
        if ( by == null || by .equals("")){
            return listCommentMapper.selectAllCommentByListId(listId);
        } else {
            return listCommentMapper.selectAllCommentByListIdOrderByUp(listId);
        }

    }

    @Override
    public boolean upOrNot(Integer userId, Integer commentId) {
        int count = listCommentMapper.selectCountUserId(userId, commentId);
        if (count > 0) {
            System.out.println(count);
            return true;
        } else {
            System.out.println(count);
            return false;
        }
    }

    @Override
    @Transactional
    public boolean upAndCancel(Integer userId, Integer commentId) {
        boolean upOrNot = upOrNot(userId, commentId);
        System.out.println(upOrNot);

        try {
            if (upOrNot) {
                System.out.println("您取消了赞");
                listCommentMapper.deleteUp(userId, commentId);
                listCommentMapper.updateUpByPrimaryKey(commentId, -1);
            } else {
                System.out.println("您点了赞");
                listCommentMapper.insertUp(userId, commentId);
                listCommentMapper.updateUpByPrimaryKey(commentId, +1);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public List<ListCommentQueryVo> findListCommentReplyByParentId(Integer parentId) {
        return listCommentMapper.selectCommentReplyByParentId(parentId);
    }

    @Override
    @Transactional
    public int addChildComment(ListComment comment) {
        try {
            int i = listCommentMapper.insert(comment);
            if(i>0){
                listCommentMapper.updateReply(comment.getParentId());
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
