package com.wnmusic.service.impl;

import com.wnmusic.entity.Singer;
import com.wnmusic.mapper.SingerMapper;
import com.wnmusic.queryvo.SingerAllMsgVo;
import com.wnmusic.service.SingerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author syy
 * @date 2022/09/07
 **/
@Service
public class SingerServiceImp implements SingerService {

    SingerMapper singerMapper;

    @Autowired
    public void setSingerMapper(SingerMapper singerMapper) {
        this.singerMapper = singerMapper;
    }


    @Override
    public List<Singer> findSingerAllByCondition(String singerAddress,String singerGender) {
        return singerMapper.selectSingerAllByCondition(singerAddress,singerGender);
    }

    @Override
    public List<Singer> findSingerInlandByCondition(String singerGender) {
        return singerMapper.selectSingerInlandByCondition(singerGender);
    }

    @Override
    public List<Singer> findSingerOtherByCondition(String singerGender, String... singerAddresses) {
        return singerMapper.selectSingerOtherByCondition(singerGender, singerAddresses);
    }

    @Override
    public List<SingerAllMsgVo> selectAllSingerMsg(Integer singerId) {
        return singerMapper.selectAllSingerMsg(singerId);
    }

    @Override
    public Integer singerCountSong(Integer singerId) {
        return singerMapper.singerCountSong(singerId);
    }

    @Override
    public Integer singerCountSongAlbum(Integer singerId) {
        return singerMapper.singerCountSongAlbum(singerId);
    }

    @Override
    public int findSingerByName(String singerName) {
        return singerMapper.selectSingerByName(singerName);
    }


}
