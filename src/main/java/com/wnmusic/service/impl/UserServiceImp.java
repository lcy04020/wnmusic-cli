package com.wnmusic.service.impl;

import com.wnmusic.entity.User;
import com.wnmusic.mapper.UserMapper;
import com.wnmusic.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author syy
 * @date 2022/09/02
 **/
@Service
public class UserServiceImp implements UserService {


    UserMapper userMapper;

    @Autowired
    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public boolean userRegister(User user) {
        return userMapper.insert(user) != 0;
    }

    @Override
    public int userLogin(String userName, String userPassword) {
        User user = userMapper.selectUserByNameAndPassword(userName);
        if (!Objects.isNull(user)) {
            if (user.getPassword().equals(userPassword)) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return -1;
        }

    }

    @Override
    public User findUserAllMsg(String userName) {
        return userMapper.selectUserByNameAndPassword(userName);
    }

    @Override
    public boolean findUserByName(String username) {
        User user = userMapper.selectUserByNameAndPassword(username);

        if (Objects.isNull(user)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public User findUserByPrimaryKey(Integer userId) {
        return userMapper.selectByPrimaryKey(userId);
    }

    @Override
    public User findUserByUserName(String username) {
        return userMapper.selectUserByNameAndPassword(username);
    }

    @Override
    public Boolean modifyUser(User user) {
        return userMapper.updateUser(user);
    }
}
