package com.wnmusic.service.impl;

import com.wnmusic.entity.Song;
import com.wnmusic.mapper.SongMapper;
import com.wnmusic.queryvo.FindSongQueryVo;
import com.wnmusic.queryvo.SongDetailVo;
import com.wnmusic.queryvo.SongPlayerQueryVo;
import com.wnmusic.service.SongService;
import com.wnmusic.utils.GetRandomList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SongServiceImpl implements SongService {
    SongMapper songMapper;

    @Autowired
    public void setSongMapper(SongMapper songMapper) {
        this.songMapper = songMapper;
    }

    @Override
    public List<Song> findAll() {
        return songMapper.selectAll();
    }

    @Override
    public Song findById(Integer id) {
        return songMapper.selectById(id);
    }

    @Override
    public SongPlayerQueryVo findSongPlayerById(Integer id) {
        return songMapper.selectSongPlayerById(id);
    }

    @Override
    public List<Song> findFifteen() {
        return songMapper.selectFifteen();
    }

    @Override
    public List<Song> findOldFifteen() {
        return songMapper.selectOldFifteen();
    }

    @Override
    public List<Song> findClaFifteen() {
        return songMapper.selectClaFifteen();
    }

    @Override
    public List<Song> findFifteenByRank() {
        return songMapper.selectFifteenByRank();
    }

    @Override
    public List<Song> findBySingerName(String singerName) {
        return songMapper.selectBySingerName(singerName);
    }

    @Override
    public SongDetailVo findSongDetailVoById(Integer id) {
        return songMapper.selectSongDetailVoByKey(id);
    }

    @Override
    public List<FindSongQueryVo> fuzzyFind(String songName) {
        return songMapper.fuzzySelect(songName);
    }

    @Override
    public List<Song> randomFind() {
        List<Integer> randomList = GetRandomList.getRandomList(45, 1, 93);
        return songMapper.randomSelect(randomList);
    }

    @Override
    public List<SongPlayerQueryVo> findByListId(List<Integer> songIds) {
        return songMapper.selectByList(songIds);
    }


}
