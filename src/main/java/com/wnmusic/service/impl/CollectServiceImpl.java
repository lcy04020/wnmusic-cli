package com.wnmusic.service.impl;

import com.wnmusic.mapper.CollectMapper;
import com.wnmusic.queryvo.CollectQueryVo;
import com.wnmusic.service.CollectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author 张
* @description 针对表【collect(收藏)】的数据库操作Service实现
* @createDate 2022-09-07 12:04:40
*/
@Service
public class CollectServiceImpl implements CollectService{

    private CollectMapper collectMapper;

    @Autowired
    public void setCollectMapper(CollectMapper collectMapper) {
        this.collectMapper = collectMapper;
    }

    @Override
    public List<CollectQueryVo> selectAllCollect(String userName) {
        return collectMapper.findUserByCollect(userName);
    }

    @Override
    public List<CollectQueryVo> selectAllSongAlbum(String userName) {
        return collectMapper.findSongAlbumByCollect(userName);
    }

    @Override
    public List<CollectQueryVo> selectAllSongList(String userName) {
        return collectMapper.findSongListByCollect(userName);
    }

}




