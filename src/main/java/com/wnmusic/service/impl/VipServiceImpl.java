package com.wnmusic.service.impl;

import com.wnmusic.entity.Vip;
import com.wnmusic.mapper.VipMapper;
import com.wnmusic.service.VipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author lcy
 * @date 2022/9/9 11:08
 */
@Service
public class VipServiceImpl implements VipService {

    VipMapper vipMapper;

    @Autowired
    public void setVipMapper(VipMapper vipMapper) {
        this.vipMapper = vipMapper;
    }

    @Override
    public int removeByPrimaryKey(Integer id) {
        return vipMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int addVip(Vip record) {
        return vipMapper.insert(record);
    }

    @Override
    public Vip findByPrimaryKey(Integer id) {
        return vipMapper.selectByPrimaryKey(id);
    }

    @Override
    public Integer findStatusByUserId(Integer userId) {
        return vipMapper.selectStatusByUserId(userId);
    }

    @Override
    public Vip findVipByUserId(Integer userId) {
        return vipMapper.selectVipByUserId(userId);
    }

    @Override
    public int modifyVipByUserId(Vip vip) {
        return vipMapper.updateVipByUserId(vip);
    }
}
