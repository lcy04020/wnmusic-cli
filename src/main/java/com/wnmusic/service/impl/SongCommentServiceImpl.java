package com.wnmusic.service.impl;

import com.wnmusic.entity.SongComment;
import com.wnmusic.mapper.SongCommentMapper;
import com.wnmusic.queryvo.SongCommentQueryVo;
import com.wnmusic.service.SongCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SongCommentServiceImpl implements SongCommentService {


    private SongCommentMapper songCommentMapper;

    @Autowired
    public void setSongCommentMapper(SongCommentMapper songCommentMapper) {
        this.songCommentMapper = songCommentMapper;
    }

    @Override
    public int removeByPrimaryKey(Long id) {
        return songCommentMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int add(SongComment record) {
        return songCommentMapper.insert(record);
    }


    @Override
    public SongComment findByPrimaryKey(Long id) {
        return songCommentMapper.selectByPrimaryKey(id);
    }


    @Override
    public int modifyByPrimaryKey(SongComment record) {
        return songCommentMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<SongCommentQueryVo> findAllCommentBySongId(Integer songId, String by) {
        if ( by == null || by .equals("")){
            return songCommentMapper.selectAllCommentBySongId(songId);
        } else {
            return songCommentMapper.selectAllCommentBySongIdOrderByUp(songId);
        }

    }

    @Override
    public boolean upOrNot(Integer userId, Integer commentId) {
        int count = songCommentMapper.selectCountUserId(userId, commentId);
        if (count > 0) {
            System.out.println(count);
            return true;
        } else {
            System.out.println(count);
            return false;
        }
    }

    @Override
    @Transactional
    public boolean upAndCancel(Integer userId, Integer commentId) {
        boolean upOrNot = upOrNot(userId, commentId);
        System.out.println(upOrNot);

        try {
            if (upOrNot) {
                System.out.println("您取消了赞");
                songCommentMapper.deleteUp(userId, commentId);
                songCommentMapper.updateUpByPrimaryKey(commentId, -1);
            } else {
                System.out.println("您点了赞");
                songCommentMapper.insertUp(userId, commentId);
                songCommentMapper.updateUpByPrimaryKey(commentId, +1);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public List<SongCommentQueryVo> findSongCommentReplyByParentId(Integer parentId) {
        return songCommentMapper.selectCommentReplyByParentId(parentId);
    }

    @Override
    @Transactional
    public int addChildComment(SongComment comment) {
        try {
            int i = songCommentMapper.insert(comment);
            if(i>0){
                songCommentMapper.updateReply(comment.getParentId());
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
