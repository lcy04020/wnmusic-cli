package com.wnmusic.service.impl;

import com.wnmusic.entity.SongList;
import com.wnmusic.mapper.SongListMapper;
import com.wnmusic.queryvo.SongListPlayerQueryVo;
import com.wnmusic.mapper.SongMapper;
import com.wnmusic.queryvo.ListDetailVo;
import com.wnmusic.queryvo.SongDetailVo;
import com.wnmusic.service.SongListService;
import com.wnmusic.utils.GetRandomList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SongListServiceImpl implements SongListService {

    private SongMapper songMapper;
    @Autowired
    public void setSongMapper(SongMapper songMapper) {
        this.songMapper = songMapper;
    }

    private SongListMapper songListMapper;
    @Autowired
    public void setSongListMapper(SongListMapper songListMapper) {
        this.songListMapper = songListMapper;
    }

    @Override
    public List<SongList> findAll() {
        return songListMapper.selectAll();
    }

    @Override
    public List<SongList> findFifteen() {
        return songListMapper.selectFifteen();
    }

    @Override
    public List<SongList> findByStyle(String style) {
        return songListMapper.selectByStyle(style);
    }

    @Override
    public int deleteByPrimaryKey(Long id) {
        return songListMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(SongList record) {
        return songListMapper.insert(record);
    }

    @Override
    public int insertSelective(SongList record) {
        return songListMapper.insertSelective(record);
    }

    @Override
    public SongList selectByPrimaryKey(Long id) {
        return songListMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(SongList record) {
        return songListMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(SongList record) {
        return songListMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<SongList> queryAllSongList() {
        return songListMapper.selectAllSongList();
    }

    @Override
    public List<SongList> randomFind() {
        List<Integer> randomList = GetRandomList.getRandomList(15, 1, 80);
        return songListMapper.randomSelect(randomList);
    }

    @Override
    public SongListPlayerQueryVo findByIdToPlayer(Integer id) {
        return songListMapper.selectByIdToPlayer(id);
    }

    @Override
    public ListDetailVo findListDetailVoByKey(Integer listId) {
        ListDetailVo listDetailVo = new ListDetailVo();

        SongList songList = songListMapper.selectSongListByPrimaryKey(listId);
        listDetailVo.setSongList(songList);

        List<Integer> songIds = songListMapper.selectSongIdByListId(listId);
        if (songIds.size()>0){
            List<SongDetailVo> songDetailVos = songMapper.selectSongListDetailVoByKey(songIds);
            listDetailVo.setSongsList(songDetailVos);
        } else {
            listDetailVo.setSongsList(null);
        }
        return listDetailVo;
    }

    @Override
    public List<String> findAllDistinctStyle() {
        return songListMapper.selectAllDistinctStyle();
    }

    @Override
    public List<SongList> fuzzyFind(String songName) {
        return songListMapper.fuzzySelect(songName);
    }

    @Override
    public List<Integer> findSongIdsById(Integer id) {
        return songListMapper.selectSongIdsById(id);
    }

    @Override
    public SongList findById(Integer id) {
        return songListMapper.selectById(id);
    }

}
