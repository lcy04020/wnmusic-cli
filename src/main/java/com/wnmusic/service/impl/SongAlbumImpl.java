package com.wnmusic.service.impl;

import com.wnmusic.entity.SongAlbum;
import com.wnmusic.mapper.SongAlbumMapper;
import com.wnmusic.queryvo.FindSongAlbumQueryVo;
import com.wnmusic.queryvo.SongAlbumPlayerQueryVo;
import com.wnmusic.mapper.SongMapper;
import com.wnmusic.queryvo.AlbumDetailVo;
import com.wnmusic.queryvo.SongDetailVo;
import com.wnmusic.service.SongAlbumService;
import com.wnmusic.utils.GetRandomList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SongAlbumImpl implements SongAlbumService {

    private SongMapper songMapper;
    @Autowired
    public void setSongMapper(SongMapper songMapper) {
        this.songMapper = songMapper;
    }
    private SongAlbumMapper songAlbumMapper;
    @Autowired
    public void setSongAlbumMapper(SongAlbumMapper songAlbumMapper) {
        this.songAlbumMapper = songAlbumMapper;
    }


    @Override
    public List<SongAlbum> findAll() {
        return songAlbumMapper.selectAll();
    }

    @Override
    public List<SongAlbum> findFifteen() {
        return songAlbumMapper.selectFifteen();
    }

    @Override
    public List<SongAlbum> randomFind() {
        List<Integer> randomList = GetRandomList.getRandomList(15, 0, 100);
        return songAlbumMapper.randomSelect(randomList);
    }

    @Override
    public List<SongAlbum> findByStyle(String style) {
        return songAlbumMapper.selectByStyle(style);
    }

    @Override
    public SongAlbumPlayerQueryVo findByIdToPlayer(Integer id) {
        return songAlbumMapper.selectByIdToPlayer(id);
    }


    @Override
    public AlbumDetailVo findAlbumDetailVoByKey(Integer albumId) {
        AlbumDetailVo albumDetailVo = new AlbumDetailVo();

        SongAlbum songAlbum = songAlbumMapper.selectSongAlbumByPrimaryKey(albumId);
        albumDetailVo.setSongAlbum(songAlbum);

        List<Integer> singerId = songAlbumMapper.selectSingerIdByAlbumId(albumId);
        if (singerId.size()>0){
            albumDetailVo.setSingerId(singerId.get(0));
        } else {
            albumDetailVo.setSingerId(null);
        }


        List<String> singerName = songAlbumMapper.selectSingerNameByAlbumId(albumId);
        if (singerName.size()>0){
            albumDetailVo.setSingerName(singerName.get(0));
        } else {
            albumDetailVo.setSingerName(null);
        }

        List<Integer> songIds = songAlbumMapper.selectSongIdByAlbumId(albumId);
        if (songIds.size()>0){
            List<SongDetailVo> songDetailVos = songMapper.selectSongListDetailVoByKey(songIds);
            albumDetailVo.setSongsList(songDetailVos);
        } else {
            albumDetailVo.setSongsList(null);
        }
        return albumDetailVo;
    }

    @Override
    public List<String> findAllDistinctStyle() {
        return songAlbumMapper.selectAllDistinctStyle();
    }

    @Override
    public List<FindSongAlbumQueryVo> fuzzyFind(String songName) {
        return songAlbumMapper.fuzzySelect(songName);
    }

    @Override
    public List<Integer> findSongIdsById(Integer id) {
        return songAlbumMapper.selectSongIdsById(id);
    }

    @Override
    public SongAlbum findById(Integer id) {
        return songAlbumMapper.selectById(id);
    }
}

