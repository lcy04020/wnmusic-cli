package com.wnmusic.service.impl;

import com.wnmusic.entity.AlbumComment;
import com.wnmusic.entity.AlbumComment;
import com.wnmusic.mapper.AlbumCommentMapper;
import com.wnmusic.queryvo.AlbumCommentQueryVo;
import com.wnmusic.service.AlbumCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AlbumCommentServiceImpl implements AlbumCommentService {
    private AlbumCommentMapper albumCommentMapper;

    @Autowired
    public void setAlbumCommentMapper(AlbumCommentMapper albumCommentMapper) {
        this.albumCommentMapper = albumCommentMapper;
    }

    @Override
    public int removeByPrimaryKey(Long id) {
        return albumCommentMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int add(AlbumComment record) {
        return albumCommentMapper.insert(record);
    }


    @Override
    public AlbumComment findByPrimaryKey(Long id) {
        return albumCommentMapper.selectByPrimaryKey(id);
    }


    @Override
    public int modifyByPrimaryKey(AlbumComment record) {
        return albumCommentMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<AlbumCommentQueryVo> findAllCommentByAlbumId(Integer songId, String by) {
        if ( by == null || by .equals("")){
            return albumCommentMapper.selectAllCommentByAlbumId(songId);
        } else {
            return albumCommentMapper.selectAllCommentByAlbumIdOrderByUp(songId);
        }

    }

    @Override
    public boolean upOrNot(Integer userId, Integer commentId) {
        int count = albumCommentMapper.selectCountUserId(userId, commentId);
        if (count > 0) {
            System.out.println(count);
            return true;
        } else {
            System.out.println(count);
            return false;
        }
    }

    @Override
    @Transactional
    public boolean upAndCancel(Integer userId, Integer commentId) {
        boolean upOrNot = upOrNot(userId, commentId);
        System.out.println(upOrNot);

        try {
            if (upOrNot) {
                System.out.println("您取消了赞");
                albumCommentMapper.deleteUp(userId, commentId);
                albumCommentMapper.updateUpByPrimaryKey(commentId, -1);
            } else {
                System.out.println("您点了赞");
                albumCommentMapper.insertUp(userId, commentId);
                albumCommentMapper.updateUpByPrimaryKey(commentId, +1);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public List<AlbumCommentQueryVo> findAlbumCommentReplyByParentId(Integer parentId) {
        return albumCommentMapper.selectCommentReplyByParentId(parentId);
    }

    @Override
    @Transactional
    public int addChildComment(AlbumComment comment) {
        try {
            int i = albumCommentMapper.insert(comment);
            if(i>0){
                albumCommentMapper.updateReply(comment.getParentId());
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
