package com.wnmusic.service.impl;

import com.wnmusic.entity.VipSong;
import com.wnmusic.mapper.VipSongMapper;
import com.wnmusic.service.VipSongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author lcy
 * @date 2022/9/9 11:14
 */
@Service
public class VipSongImpl implements VipSongService {
    VipSongMapper vipSongMapper;

    @Autowired
    public void setVipSongMapper(VipSongMapper vipSongMapper) {
        this.vipSongMapper = vipSongMapper;
    }

    @Override
    public int removeByPrimaryKey(Integer id) {
        return vipSongMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int addVipSong(VipSong record) {
        return vipSongMapper.insert(record);
    }

    @Override
    public VipSong findByPrimaryKey(Integer id) {
        return vipSongMapper.selectByPrimaryKey(id);
    }

    @Override
    public int modifyByPrimaryKey(VipSong record) {
        return vipSongMapper.updateByPrimaryKey(record);
    }

    @Override
    public Integer findStatusBySongId(Integer songId) {
        return vipSongMapper.selectStatusBySongId(songId);
    }
}
