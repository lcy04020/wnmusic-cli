package com.wnmusic.service.impl;

import com.wnmusic.mapper.RankMapper;
import com.wnmusic.queryvo.SongAlbumRankQueryVo;
import com.wnmusic.queryvo.SongListRankQueryVo;
import com.wnmusic.queryvo.SongRankQueryVo;
import com.wnmusic.service.RankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RankServiceImpl implements RankService {
    RankMapper rankMapper;

    @Autowired
    public void setRankMapper(RankMapper rankMapper) {
        this.rankMapper = rankMapper;
    }

    @Override
    public List<SongRankQueryVo> findAllSongRankAvg() {
        return rankMapper.selectAllSongRankAvg();
    }

    @Override
    public List<SongRankQueryVo> findAllSongRankAvgAndTime() {
        return rankMapper.selectAllSongRankAvgAndTime();
    }

    @Override
    public List<SongListRankQueryVo> findAllSongListRankAvg() {
        return rankMapper.selectAllSongListRankAvg();
    }

    @Override
    public List<SongListRankQueryVo> findAllSongListRankAvgByStyle(String style) {
        return rankMapper.selectAllSongListRankAvgByStyle(style);
    }

    @Override
    public List<SongAlbumRankQueryVo> findAllSongAlbumRankAvg() {
        return rankMapper.selectAllSongAlbumRankAvg();
    }
}
