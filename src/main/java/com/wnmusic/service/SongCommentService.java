package com.wnmusic.service;

import com.wnmusic.entity.SongComment;
import com.wnmusic.queryvo.SongCommentQueryVo;

import java.util.List;

public interface SongCommentService {

    int removeByPrimaryKey(Long id);

    int add(SongComment record);

    SongComment findByPrimaryKey(Long id);

    int modifyByPrimaryKey(SongComment record);

    List<SongCommentQueryVo> findAllCommentBySongId(Integer songId, String by);

    boolean upOrNot(Integer userId, Integer commentId);

    boolean upAndCancel(Integer userId, Integer commentId);

    List<SongCommentQueryVo> findSongCommentReplyByParentId(Integer parentId);

    int addChildComment(SongComment comment);
}
