package com.wnmusic.service;

import com.wnmusic.entity.VipSong;

/**
 * @author lcy
 * @date 2022/9/9 11:13
 */
public interface VipSongService {
    int removeByPrimaryKey(Integer id);

    int addVipSong(VipSong record);

    VipSong findByPrimaryKey(Integer id);

    int modifyByPrimaryKey(VipSong record);

    Integer findStatusBySongId(Integer songId);
}
