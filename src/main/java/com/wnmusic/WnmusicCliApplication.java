package com.wnmusic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan("com.wnmusic.listener")
public class WnmusicCliApplication {

    public static void main(String[] args) {
        SpringApplication.run(WnmusicCliApplication.class, args);
    }

}
