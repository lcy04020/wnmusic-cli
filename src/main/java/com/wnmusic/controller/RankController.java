package com.wnmusic.controller;

import com.wnmusic.common.Result;
import com.wnmusic.entity.SongAlbum;
import com.wnmusic.queryvo.SongAlbumRankQueryVo;
import com.wnmusic.queryvo.SongListRankQueryVo;
import com.wnmusic.queryvo.SongRankQueryVo;
import com.wnmusic.service.RankService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.oas.annotations.EnableOpenApi;

import java.util.List;
import java.util.Objects;

@EnableOpenApi
@RestController
@ApiModel(value = "排行管理")
@RequestMapping("/rank")
public class RankController {

    RankService rankService;

    @Autowired
    public void setRankService(RankService rankService) {
        this.rankService = rankService;
    }

    @ApiOperation("歌曲排行")
    @GetMapping("/songRank")
    public Result findSongRank(){
        List<SongRankQueryVo> songRankAvg = rankService.findAllSongRankAvg();
        if(!Objects.isNull(songRankAvg)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("songRank",songRankAvg);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("songRank", null);
        }
    }

    @ApiOperation("最新歌曲排行")
    @GetMapping("/newSongRank")
    public Result findNewSongRank(){
        List<SongRankQueryVo> songRankAvg = rankService.findAllSongRankAvgAndTime();
        if(!Objects.isNull(songRankAvg)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("songRankAndTime",songRankAvg);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("songRankAndTime", null);
        }
    }

    @ApiOperation("歌单排行")
    @GetMapping("/songListRank")
    public Result findSongListRank(){
        List<SongListRankQueryVo> songListRankAvg = rankService.findAllSongListRankAvg();
        if(!Objects.isNull(songListRankAvg)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("songListRank",songListRankAvg);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("songListRank", null);
        }
    }

    @ApiOperation("不同风格歌单排行")
    @GetMapping("/songListRankByStyle")
    public Result findSongListRankByStyle(String style){
        List<SongListRankQueryVo> songListRankAvgByStyle = rankService.findAllSongListRankAvgByStyle(style);
        if(!Objects.isNull(songListRankAvgByStyle)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("songListRankByStyle",songListRankAvgByStyle);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("songListRankByStyle", null);
        }
    }

    @ApiOperation("专辑排行")
    @GetMapping("/songAlbumRank")
    public Result findSongAlbumRank(){
        List<SongAlbumRankQueryVo> songAlbumRankAvg = rankService.findAllSongAlbumRankAvg();
        if(!Objects.isNull(songAlbumRankAvg)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("songAlbumRank",songAlbumRankAvg);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("songAlbumRank", null);
        }
    }
}
