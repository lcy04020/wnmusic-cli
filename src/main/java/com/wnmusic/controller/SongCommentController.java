package com.wnmusic.controller;

import com.wnmusic.common.Result;
import com.wnmusic.common.TokenUtils;
import com.wnmusic.entity.SongComment;
import com.wnmusic.entity.User;
import com.wnmusic.queryvo.SongCommentQueryVo;
import com.wnmusic.service.SongCommentService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.oas.annotations.EnableOpenApi;

import java.util.List;

@EnableOpenApi
@RestController
@ApiModel(value = "歌曲评论管理")
@RequestMapping("/SongComment")
public class SongCommentController {

    private SongCommentService songCommentService;

    private TokenUtils tokenUtils;

    @Autowired
    public void setSongCommentService(SongCommentService songCommentService) {
        this.songCommentService = songCommentService;
    }

    @Autowired
    public void setTokenUtils(TokenUtils tokenUtils) {
        this.tokenUtils = tokenUtils;
    }

    @ApiOperation("新增评论")
    @PostMapping("/addSongComment")
    public Result addSongComment(@RequestParam("userId") Integer userId,
                                 @RequestParam("songId") Integer songId,
                                 @RequestParam("content") String content,
                                 @RequestParam("parentId") Integer parentId,
                                 @RequestParam("token") String token
    ) {
        User currentUser1 = tokenUtils.getCurrentUser1(token);
        SongComment comment = new SongComment();
        comment.setUserId(currentUser1.getId());
        comment.setSongId(songId);
        comment.setContent(content);
        comment.setParentId(parentId);
        if (parentId != null && parentId != 0) {
            int i = songCommentService.addChildComment(comment);
            if (i > 0) {
                return Result.resultBuilder().state(200).result(true).description("发表成功");
            } else {
                return Result.resultBuilder().state(500).result(false).description("发表失败");
            }
        } else {
            int i = songCommentService.add(comment);
            if (i > 0) {
                return Result.resultBuilder().state(200).result(true).description("发表成功");
            } else {
                return Result.resultBuilder().state(500).result(false).description("发表失败");
            }
        }
    }

    @ApiOperation("加载已有的歌曲评论（不包括子回复）")
    @PostMapping("/findAllSongComment")
    @ResponseBody
    public Result findAllComment(@RequestParam("songId")Integer songId,
                                 @RequestParam("by")String by) {
        System.out.println(by+"排序");
        List<SongCommentQueryVo> allComment = songCommentService.findAllCommentBySongId(songId, by);
        return Result.resultBuilder().state(200).data("allComment", allComment);
    }

    @ApiOperation("加载评论的子回复")
    @PostMapping("/findSongCommentReply")
    @ResponseBody
    public Result loadSongCommentReply(Integer parentId) {
        List<SongCommentQueryVo> commentReply = songCommentService.findSongCommentReplyByParentId(parentId);
        return Result.resultBuilder().state(200).data("commentReply", commentReply);
    }


    @ApiOperation("给评论点赞/取消赞")
    @PostMapping("/commentUp")
    @ResponseBody
    public Result giveCommentUp(@RequestParam("userId") Integer userId,
                                @RequestParam("commentId") Integer commentId,
                                @RequestParam("token") String token) {
        User currentUser1 = tokenUtils.getCurrentUser1(token);
        boolean upAndCancel = songCommentService.upAndCancel(currentUser1.getId(), commentId);
        return Result.resultBuilder().state(200).data("upAndCancel", upAndCancel);
    }

    @ApiOperation("确认：你点赞了吗")
    @PostMapping("/upOrNot")
    @ResponseBody
    public Result upOrNot(@RequestParam("userId") Integer userId,
                          @RequestParam("commentId") Integer commentId,
                          @RequestParam("token") String token) {
        User currentUser1 = tokenUtils.getCurrentUser1(token);
        boolean upOrNot = songCommentService.upOrNot(currentUser1.getId(), commentId);
        return Result.resultBuilder().state(200).data("upOrNot", upOrNot);
    }

}
