package com.wnmusic.controller;

import com.wnmusic.queryvo.SingerAllMsgVo;
import com.wnmusic.service.SingerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author syy
 * @date 2022/09/07
 **/
@Controller
@RequestMapping("/singerDetail")
public class SingerDetailController {

    private SingerService singerService;

    @Autowired
    public void setSingerService(SingerService singerService) {
        this.singerService = singerService;
    }

    @GetMapping("/findSingerById/{id}")
    public String findSingerById(@PathVariable("id") int singerId, Model model){

        List<SingerAllMsgVo> singerAllMsgVoList = singerService.selectAllSingerMsg(singerId);
        for (SingerAllMsgVo singerAllMsgVo : singerAllMsgVoList) {
            System.out.println(singerAllMsgVo);
        }
        Integer countSong = singerService.singerCountSong(singerId);
        System.out.println(countSong);
        Integer countSongAlbum = singerService.singerCountSongAlbum(singerId);
        System.out.println(countSongAlbum);
        model.addAttribute("singerAllMsgVoList",singerAllMsgVoList);
        model.addAttribute("countSong",countSong);
        model.addAttribute("countSongAlbum",countSongAlbum);

        return "singerDetail";
    }

    @GetMapping("/findSingerByName")
    public String findSingerByName(String singerName, Model model){
        int singerId = singerService.findSingerByName(singerName);
        List<SingerAllMsgVo> singerAllMsgVoList = singerService.selectAllSingerMsg(singerId);
        for (SingerAllMsgVo singerAllMsgVo : singerAllMsgVoList) {
            System.out.println(singerAllMsgVo);
        }
        Integer countSong = singerService.singerCountSong(singerId);
        System.out.println(countSong);
        Integer countSongAlbum = singerService.singerCountSongAlbum(singerId);
        System.out.println(countSongAlbum);
        model.addAttribute("singerAllMsgVoList",singerAllMsgVoList);
        model.addAttribute("countSong",countSong);
        model.addAttribute("countSongAlbum",countSongAlbum);

        return "singerDetail";
    }
}
