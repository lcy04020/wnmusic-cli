package com.wnmusic.controller;

import com.alibaba.fastjson.JSON;
import com.sun.org.apache.xpath.internal.operations.Mod;
import com.wnmusic.common.Result;
import com.wnmusic.common.TokenUtils;
import com.wnmusic.entity.Song;
import com.wnmusic.entity.SongAlbum;
import com.wnmusic.entity.SongList;
import com.wnmusic.entity.User;
import com.wnmusic.queryvo.FindSongQueryVo;
import com.wnmusic.queryvo.SongDetailVo;
import com.wnmusic.queryvo.SongListPlayerQueryVo;
import com.wnmusic.queryvo.SongPlayerQueryVo;
import com.wnmusic.service.SongListService;
import com.wnmusic.service.SongService;
import com.wnmusic.utils.GetRandomList;
import com.wnmusic.utils.GetSongTime;
import com.wnmusic.utils.SplitLyric;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.oas.annotations.EnableOpenApi;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@EnableOpenApi
@Controller
@ApiModel(value = "歌曲管理")
@RequestMapping("/song")
public class SongController {
    private SongService songService;

    private StringRedisTemplate redisTemplate;

    private TokenUtils tokenUtils;

    @Autowired
    public void setSongListService(SongService songService) {
        this.songService = songService;
    }

    @Autowired
    public void setRedisTemplate(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Autowired
    public void setTokenUtils(TokenUtils tokenUtils) {
        this.tokenUtils = tokenUtils;
    }

    @ApiOperation("查找全部")
    @GetMapping("/findAll")
    public @ResponseBody Result findAll(){
        List<Song> list = songService.findAll();
        if(!Objects.isNull(list)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("list",list);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("list", null);
        }
    }

    @ApiOperation("根据风格查找")
    @GetMapping("/findByStyle")
    public @ResponseBody Result findBySingerName(String singerName){
        List<Song> list = songService.findBySingerName(singerName);
        if(!Objects.isNull(list)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("list",list);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("list", null);
        }
    }

    @ApiOperation("根据Id查找,跳转到播放页面")
    @GetMapping("/findByIdToPlayer")
    public String findById(Integer id, Model model){
        String obj = redisTemplate.opsForValue().get(id+"");
        String songTime = redisTemplate.opsForHash().get("allSongTime", id + "").toString();
        SongPlayerQueryVo song = JSON.parseObject(obj, SongPlayerQueryVo.class);
        song.setSongTime(songTime);
        model.addAttribute("song",song);
        model.addAttribute("songLyric",song.getSongLyric());
        model.addAttribute("type","song");
        return "songPlayer";
    }

    @ApiOperation("随机查找45个")
    @GetMapping("/randomFind")
    public @ResponseBody Result randomFind(){
        //List<Song> list = songService.randomFind();
        List<Integer> randomList = GetRandomList.getRandomList(45, 1, 93);
        List<Song> songs = new ArrayList<>();
        for (int i = 0; i < randomList.size(); i++) {
            String song = Objects.requireNonNull(redisTemplate.opsForHash().get("allSong", randomList.get(i) + "")).toString();
            songs.add(JSON.parseObject(song, Song.class));
        }
        if(!Objects.isNull(songs)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("list",songs);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("list", null);
        }
    }

    @ApiOperation("查前45条")
    @GetMapping("/findFifteen")
    public @ResponseBody Result findFifteen(){
        List<Song> list = songService.findFifteen();
        if(!Objects.isNull(list)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("list",list);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("list", null);
        }
    }

    @ApiOperation("根据评分查前45条")
    @GetMapping("/findFifteenByRank")
    public @ResponseBody Result findFifteenByRank(){
        List<Song> list = songService.findFifteenByRank();
        if(!Objects.isNull(list)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("list",list);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("list", null);
        }
    }

    @ApiOperation("查45条老歌")
    @GetMapping("/findOldFifteen")
    public @ResponseBody Result findOldFifteen(){
        List<Song> list = songService.findOldFifteen();
        if(!Objects.isNull(list)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("list",list);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("list", null);
        }
    }

    @ApiOperation("查45条经典")
    @GetMapping("/findClaFifteen")
    public @ResponseBody Result findClaFifteen(){
        List<Song> list = songService.findClaFifteen();
        if(!Objects.isNull(list)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("list",list);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("list", null);
        }
    }

    @ApiOperation("模糊查询歌曲")
    @GetMapping("/fuzzyFind")
    public String fuzzyFind(String songName, Model model,String token){
        User currentUser = tokenUtils.getCurrentUser1(token);
        int userId = currentUser.getId();
        List<FindSongQueryVo> findSongQueryVos = songService.fuzzyFind(songName);
        model.addAttribute("list",findSongQueryVos);
        model.addAttribute("name",songName);
        model.addAttribute("type","song");
        List<SongPlayerQueryVo> songs = new ArrayList<>();
        SongListPlayerQueryVo songListPlayerQueryVo = new SongListPlayerQueryVo();
        for (FindSongQueryVo findSongQueryVo : findSongQueryVos) {
            String s = redisTemplate.opsForValue().get(findSongQueryVo.getId() + "");
            String songTime = redisTemplate.opsForHash().get("allSongTime", findSongQueryVo.getId() + "").toString();
            SongPlayerQueryVo song = JSON.parseObject(s, SongPlayerQueryVo.class);
            song.setSongTime(songTime);
            songs.add(song);
            songListPlayerQueryVo.setList(songs);
        }
        redisTemplate.opsForValue().set("songList"+userId,JSON.toJSONString(songListPlayerQueryVo));
        if(songs.size()>0){
            String id = songs.get(0).getId().toString();
            List<String> historySong = redisTemplate.opsForList().range("historySong"+userId, 0, -1);
            boolean flag = true;
            for (String s : historySong) {
                if(id.equals(s)){
                    flag = false;
                }
            }
            if(flag){
                redisTemplate.opsForList().leftPush("historySong"+userId,id);
                redisTemplate.expire("history",14400, TimeUnit.SECONDS);
            }
            historySong = redisTemplate.opsForList().range("historySong"+userId, 0, -1);
            if(historySong.size()==6){
                redisTemplate.opsForList().rightPop("historySong"+userId);
            }
        }
        return "find";
    }

    @ApiOperation("查前歌曲详情")
    @GetMapping("/findSongDetail")
    public String findSongDetail(@RequestParam Integer id, Model model) {
        SongDetailVo songDetailVo = songService.findSongDetailVoById(id);
        model.addAttribute("songDetailVo",songDetailVo);
        return "songDetail";
    }

    @ApiOperation("从查找页面到播放页面")
    @GetMapping("/findToPlaySong")
    public String findToPlaySong(Model model,String token){
        User currentUser = tokenUtils.getCurrentUser1(token);
        int userId = currentUser.getId();
        String findSongs = redisTemplate.opsForValue().get("songList"+userId);
        SongListPlayerQueryVo songList = JSON.parseObject(findSongs, SongListPlayerQueryVo.class);
        model.addAttribute("type","songList");
        model.addAttribute("songList",songList);
        model.addAttribute("song", songList.getList().get(0));
        return "songPlayer";
    }

    @ApiOperation("展示历史查询")
    @PostMapping("/showHistoryFindSong")
    public @ResponseBody Result showHistoryFindSong(String token){
        User currentUser = tokenUtils.getCurrentUser1(token);
        int userId = currentUser.getId();
        List<String> historySongId = redisTemplate.opsForList().range("historySong"+userId, 0, -1);
        List<SongPlayerQueryVo> songPlayerQueryVos = new ArrayList<>();
        for (String s : historySongId) {
            String historySong = redisTemplate.opsForValue().get(s);
            SongPlayerQueryVo songPlayerQueryVo = JSON.parseObject(historySong, SongPlayerQueryVo.class);
            songPlayerQueryVos.add(songPlayerQueryVo);
        }
        if(!Objects.isNull(songPlayerQueryVos)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("list",songPlayerQueryVos);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("list", null);
        }
    }

    @ApiOperation("删除历史记录")
    @GetMapping("/removeHistory")
    public @ResponseBody Result removeHistory(String id,String token){
        User currentUser = tokenUtils.getCurrentUser1(token);
        int userId = currentUser.getId();
        Long history = redisTemplate.opsForList().remove("historySong"+userId, 1, id);
        return Result.resultBuilder().state(200).result(true).description("移除成功");
    }

    @ApiOperation("从歌手页面播放该歌手所有歌曲")
    @GetMapping("/findAllSongOfOneSingerToPlayer")
    public String findAllSongOfOneSingerToPlayer(String singerName,Model model){
//        String findSongs = redisTemplate.opsForValue().get("songList");
        List<SongPlayerQueryVo> list = new ArrayList<>();
        List<Song> songs = songService.findBySingerName(singerName);
        for (Song song : songs) {
            String s = redisTemplate.opsForValue().get(song.getId() + "");
            String songTime = redisTemplate.opsForHash().get("allSongTime", song.getId() + "").toString();
            SongPlayerQueryVo songPlayerQueryVo = JSON.parseObject(s, SongPlayerQueryVo.class);
            songPlayerQueryVo.setSongTime(songTime);
            list.add(songPlayerQueryVo);
        }
        SongListPlayerQueryVo songListPlayerQueryVo = new SongListPlayerQueryVo();
        songListPlayerQueryVo.setList(list);
        model.addAttribute("type","songList");
        model.addAttribute("songList",songListPlayerQueryVo);
        model.addAttribute("song", list.get(0));
        redisTemplate.opsForValue().set("songList",JSON.toJSONString(songListPlayerQueryVo));
        return "songPlayer";
    }

}
