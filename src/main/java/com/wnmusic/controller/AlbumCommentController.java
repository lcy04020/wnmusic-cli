package com.wnmusic.controller;

import com.wnmusic.common.Result;
import com.wnmusic.common.TokenUtils;
import com.wnmusic.entity.AlbumComment;
import com.wnmusic.entity.User;
import com.wnmusic.queryvo.AlbumCommentQueryVo;
import com.wnmusic.service.AlbumCommentService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.oas.annotations.EnableOpenApi;

import java.util.List;

@EnableOpenApi
@RestController
@ApiModel(value = "专辑评论管理")
@RequestMapping("/AlbumComment")
public class AlbumCommentController {

    private AlbumCommentService albumCommentService;

    private TokenUtils tokenUtils;

    @Autowired
    public void setAlbumCommentService(AlbumCommentService albumCommentService) {
        this.albumCommentService = albumCommentService;
    }


    @Autowired
    public void setTokenUtils(TokenUtils tokenUtils) {
        this.tokenUtils = tokenUtils;
    }


    @ApiOperation("新增评论")
    @PostMapping("/addAlbumComment")
    public Result addAlbumComment(@RequestParam("userId") Integer userId,
                                 @RequestParam("albumId") Integer albumId,
                                 @RequestParam("content") String content,
                                 @RequestParam("parentId") Integer parentId,
                                  @RequestParam("token") String token
    ) {
        User currentUser1 = tokenUtils.getCurrentUser1(token);
        AlbumComment comment = new AlbumComment();
        comment.setUserId(currentUser1.getId());
        comment.setAlbumId(albumId);
        comment.setContent(content);
        comment.setParentId(parentId);
        if (parentId != null && parentId != 0) {
            int i = albumCommentService.addChildComment(comment);
            if (i > 0) {
                return Result.resultBuilder().state(200).result(true).description("发表成功");
            } else {
                return Result.resultBuilder().state(500).result(false).description("发表失败");
            }
        } else {
            int i = albumCommentService.add(comment);
            if (i > 0) {
                return Result.resultBuilder().state(200).result(true).description("发表成功");
            } else {
                return Result.resultBuilder().state(500).result(false).description("发表失败");
            }
        }
    }

    @ApiOperation("加载已有的歌曲评论（不包括子回复）")
    @PostMapping("/findAllAlbumComment")
    @ResponseBody
    public Result findAllComment(@RequestParam("albumId")Integer albumId,
                                 @RequestParam("by")String by) {
        System.out.println(by+"排序");
        List<AlbumCommentQueryVo> allComment = albumCommentService.findAllCommentByAlbumId(albumId, by);
        return Result.resultBuilder().state(200).data("allComment", allComment);
    }

    @ApiOperation("加载评论的子回复")
    @PostMapping("/findAlbumCommentReply")
    @ResponseBody
    public Result loadAlbumCommentReply(Integer parentId) {
        List<AlbumCommentQueryVo> commentReply = albumCommentService.findAlbumCommentReplyByParentId(parentId);
        return Result.resultBuilder().state(200).data("commentReply", commentReply);
    }


    @ApiOperation("给评论点赞/取消赞")
    @PostMapping("/commentUp")
    @ResponseBody
    public Result giveCommentUp(@RequestParam("userId") Integer userId,
                                @RequestParam("commentId") Integer commentId,
                                @RequestParam("token") String token) {
        User currentUser1 = tokenUtils.getCurrentUser1(token);
        boolean upAndCancel = albumCommentService.upAndCancel(currentUser1.getId(), commentId);
        return Result.resultBuilder().state(200).data("upAndCancel", upAndCancel);
    }

    @ApiOperation("确认：你点赞了吗")
    @PostMapping("/upOrNot")
    @ResponseBody
    public Result upOrNot(@RequestParam("userId") Integer userId,
                          @RequestParam("commentId") Integer commentId,
                          @RequestParam("token") String token) {
        User currentUser1 = tokenUtils.getCurrentUser1(token);
        boolean upOrNot = albumCommentService.upOrNot(currentUser1.getId(), commentId);
        return Result.resultBuilder().state(200).data("upOrNot", upOrNot);
    }

}
