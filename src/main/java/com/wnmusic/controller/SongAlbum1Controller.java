package com.wnmusic.controller;


import com.wnmusic.queryvo.AlbumDetailVo;
import com.wnmusic.service.SongAlbumService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import springfox.documentation.oas.annotations.EnableOpenApi;

import java.util.List;


@EnableOpenApi
@Controller
@ApiModel(value = "专辑管理")
@RequestMapping("/songAlbum")
public class SongAlbum1Controller {


    private SongAlbumService songAlbumService;

    @Autowired
    public void setSongAlbumService(SongAlbumService songAlbumService) {
        this.songAlbumService = songAlbumService;
    }


    @ApiOperation("根据主键查找专辑")
    @RequestMapping("/findAlbumByKey")
    public String findAlbumByKey(@RequestParam("id") Integer id, Model model) {
        AlbumDetailVo albumDetailVo = songAlbumService.findAlbumDetailVoByKey(id);
        model.addAttribute("albumDetailVo", albumDetailVo);
        return "albumDetail";
    }
}
