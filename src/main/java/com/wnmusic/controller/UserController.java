package com.wnmusic.controller;

import com.alibaba.fastjson.JSONObject;
import com.wnmusic.common.Result;
import com.wnmusic.common.TokenUtils;
import com.wnmusic.entity.User;
import com.wnmusic.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * @author syy
 * @date 2022/09/02
 **/
@RestController
@RequestMapping("/user")
public class UserController {

    private UserService userService;
    private TokenUtils tokenUtils;
    private StringRedisTemplate redisTemplate;

    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    public void setRedisTemplate(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Autowired
    public void setTokenUtils(TokenUtils tokenUtils) {
        this.tokenUtils = tokenUtils;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/login")
    public Result userLogin(@RequestParam("userName") String userName,
                            @RequestParam("userPassword") String userPassword) {
        System.out.println(userName);
        System.out.println(userPassword);
        int i = userService.userLogin(userName, userPassword);
        Result result = null;
        if (i == 1) {

            User user1 = userService.findUserAllMsg(userName);
            System.out.println(user1);
            //-- 生成Token
            User user = userService.findUserByUserName(userName);
            String token = tokenUtils.genToken(user.getId().toString(), userPassword);
            logger.debug("token:" + token);
            redisTemplate.opsForValue().set(token, token, 2, TimeUnit.HOURS);
            //-- 把用户名喝token放到响应中
            result = Result.resultBuilder()
                    .result(true)
                    .description("登陆成功")
                    .state(200)
                    .data("userName", userName)
                    .data("user",user1)
                    .data("i", i)
                    .data("Authorization", token);
        } else if (i == -1) {
            result = Result.resultBuilder().result(false).description("用户名错误").state(500).data("i", i);
        } else {
            result = Result.resultBuilder().result(false).description("密码错误").state(500).data("i", i);
        }
        return result;
    }

    @PostMapping("/register")
    public Result userRegister(User user) {
        return Result.resultBuilder().result(userService.userRegister(user));
    }


    @GetMapping("/checkName")
    private Object checkName(@RequestParam("username") String username) {
        System.out.println("这个name就是我们要验证的新用户名" + username);

        // 查找数据库有没有这个名字
        boolean f = userService.findUserByName(username);


        // 使用了bootstrapValidaor，得到消息
        HashMap<String, Object> map = new HashMap<>();
        map.put("valid", f);
        JSONObject.toJSON(map);
        return JSONObject.toJSON(map);
    }
}
