package com.wnmusic.controller;

import com.wnmusic.common.Result;
import com.wnmusic.entity.Singer;
import com.wnmusic.service.SingerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author syy
 * @date 2022/09/07
 **/
@RestController
@RequestMapping("/singer")
public class SingerController {

    private SingerService singerService;

    @Autowired
    public void setSingerService(SingerService singerService) {
        this.singerService = singerService;
    }

    @GetMapping("/findSingerByCondition")
    public Result findSingerByCondition(
            @RequestParam(name = "singerGender", defaultValue = "") String singerGender,
            @RequestParam(name = "singerAddress", defaultValue = "") String singerAddress) {

        System.out.println("性别" + singerGender);
        System.out.println("地址" + singerAddress);

        List<Singer> singerList = new ArrayList<>();
        if ("全部".equals(singerAddress)) {
            singerAddress = "";
            if ("全部".equals(singerGender)) {
                singerGender = "";
            }
            singerList = singerService.findSingerAllByCondition(singerAddress, singerGender);
        }else if("内地".equals(singerAddress)){
            if ("全部".equals(singerGender)) {
                singerGender = "";
            }
            singerList = singerService.findSingerInlandByCondition(singerGender);
        }else if("港台".equals(singerAddress)){
            if ("全部".equals(singerGender)) {
                singerGender = "";
            }
            singerList = singerService.findSingerOtherByCondition(singerGender, "中国香港","中国台湾");
        }else if("欧美".equals(singerAddress)){
            if ("全部".equals(singerGender)) {
                singerGender = "";
            }
            singerList = singerService.findSingerOtherByCondition(singerGender,"美国","欧洲","意大利","新加波","马来西亚","西班牙",
                    "加拿大","德国","英国","挪威","瑞典" );
        }else if("日本".equals(singerAddress)){
            if ("全部".equals(singerGender)) {
                singerGender = "";
            }
            singerList = singerService.findSingerOtherByCondition(singerGender,"日本" );
        }else if("韩国".equals(singerAddress)){
            if ("全部".equals(singerGender)) {
                singerGender = "";
            }
            singerList = singerService.findSingerOtherByCondition(singerGender,"韩国" );
        }


        System.out.println(singerList.toString());
        return Result.resultBuilder().state(200).result(true).description("查找成功").data("singerList", singerList);
    }
}
