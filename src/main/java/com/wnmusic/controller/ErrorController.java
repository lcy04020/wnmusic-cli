package com.wnmusic.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author lcy
 * @date 2022/9/12 11:39
 */
@Controller
public class ErrorController {
    @RequestMapping(value = "/404")
    public String error404(){
        return "404";
    }

    @RequestMapping(value = "/500")
    public String error500(){
        return "500";
    }
}
