package com.wnmusic.controller;

import com.alibaba.fastjson.JSON;
import com.wnmusic.common.Result;
import com.wnmusic.common.TokenUtils;
import com.wnmusic.entity.SongList;
import com.wnmusic.entity.User;
import com.wnmusic.queryvo.FindSongQueryVo;
import com.wnmusic.queryvo.SongListPlayerQueryVo;
import com.wnmusic.queryvo.SongPlayerQueryVo;
import com.wnmusic.service.SongListService;
import com.wnmusic.utils.GetRandomList;
import com.wnmusic.utils.GetSongTime;
import com.wnmusic.utils.SplitLyric;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.oas.annotations.EnableOpenApi;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@EnableOpenApi
@Controller
@ApiModel(value = "歌单管理")
@RequestMapping("/songList")
public class SongListController {
    private SongListService songListService;

    private StringRedisTemplate redisTemplate;

    private TokenUtils tokenUtils;

    @Autowired
    public void setSongListService(SongListService songListService) {
        this.songListService = songListService;
    }

    @Autowired
    public void setRedisTemplate(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Autowired
    public void setTokenUtils(TokenUtils tokenUtils) {
        this.tokenUtils = tokenUtils;
    }

    @ApiOperation("查找全部")
    @GetMapping("/findAll")
    public @ResponseBody Result findAll(){
        List<SongList> list = songListService.findAll();
        if(!Objects.isNull(list)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("songList",list);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("songList", null);
        }
    }

    @ApiOperation("随机查找15个")
    @GetMapping("/randomFind")
    public @ResponseBody Result randomFind(){
//        List<SongList> list = songListService.randomFind();
        List<Integer> randomList = GetRandomList.getRandomList(15, 1, 84);
        List<SongList> songLists = new ArrayList<>();
        for (int i = 0; i < randomList.size(); i++) {
            String songList = Objects.requireNonNull(redisTemplate.opsForHash().get("allSongList", randomList.get(i) + "")).toString();
            songLists.add(JSON.parseObject(songList, SongList.class));
        }
        if(!Objects.isNull(songLists)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("songList",songLists);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("songList", null);
        }
    }

    @ApiOperation("根据风格查找")
    @PostMapping("/findByStyle")
    public @ResponseBody Result findByStyle(String style){
        List<SongList> list = songListService.findByStyle(style);
        if(!Objects.isNull(list)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("songList",list);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("songList", null);
        }
    }

    @ApiOperation("查前45条")
    @GetMapping("/findFifteen")
    public @ResponseBody Result findFifteen(){
        List<SongList> list = songListService.findFifteen();
        if(!Objects.isNull(list)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("songList",list);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("songList", null);
        }
    }

    @ApiOperation("根据id查找个歌单歌曲，并跳转到播放页面")
    @GetMapping("/findByIdToPlayer")
    public String findByIdToPlayer(Integer id, Model model, String token) throws IOException {
        int userId = 0;
        if(token!=null) {
            User currentUser = tokenUtils.getCurrentUser1(token);
            userId = currentUser.getId();
        }
        SongListPlayerQueryVo songListPlayerQueryVo = new SongListPlayerQueryVo();
        List<Integer> songIds = songListService.findSongIdsById(id);
        List<SongPlayerQueryVo> songs = new ArrayList<>();
        for (Integer songId : songIds) {
            String s = redisTemplate.opsForValue().get(songId + "");
            String songTime = redisTemplate.opsForHash().get("allSongTime", songId + "").toString();
            SongPlayerQueryVo playerQueryVo = JSON.parseObject(s, SongPlayerQueryVo.class);
            playerQueryVo.setSongTime(songTime);
            songs.add(playerQueryVo);
        }
        songListPlayerQueryVo.setList(songs);
        model.addAttribute("type","songList");
        model.addAttribute("songList",songListPlayerQueryVo);
        model.addAttribute("song", songs.get(0));
        redisTemplate.opsForValue().set("songList"+userId,JSON.toJSONString(songListPlayerQueryVo));
        return "songPlayer";
    }

    @ApiOperation("切上一首歌")
    @GetMapping("/preSong")
    public @ResponseBody Result preSong(Integer songId, Model model,String token){
        User currentUser = tokenUtils.getCurrentUser1(token);
        int userId = currentUser.getId();
        String RedisSongList = redisTemplate.opsForValue().get("songList"+userId);
        SongListPlayerQueryVo songList = JSON.parseObject(RedisSongList, SongListPlayerQueryVo.class);
        List<SongPlayerQueryVo> queryVos = songList.getList();
        int index = 0;
        for (SongPlayerQueryVo queryVo : queryVos) {
            if(queryVo.getId() == songId){
                index = queryVos.indexOf(queryVo);
            }
        }
        songList.setList(queryVos);
        model.addAttribute("type","songList");
        model.addAttribute("songList",songList);
        if(index == 0){
            index = queryVos.size()-1;
        }else{
            index -= 1;
        }
        int newSongId = queryVos.get(index).getId();
        String songTime = redisTemplate.opsForHash().get("allSongTime", newSongId + "").toString();
        queryVos.get(index).setSongTime(songTime);
        model.addAttribute("song", queryVos.get(index));
        return Result.resultBuilder().state(200).result(true).description("切换上一曲成功").data("song",queryVos.get(index));
    }

    @ApiOperation("切下一首歌")
    @GetMapping("/nextSong")
    public @ResponseBody Result nextSong(Integer songId, Model model,String token){
        User currentUser = tokenUtils.getCurrentUser1(token);
        int userId = currentUser.getId();
        String RedisSongList = redisTemplate.opsForValue().get("songList"+userId);
        SongListPlayerQueryVo songList = JSON.parseObject(RedisSongList, SongListPlayerQueryVo.class);
        List<SongPlayerQueryVo> queryVos = songList.getList();
        int index = 0;
        for (SongPlayerQueryVo queryVo : queryVos) {
            if(queryVo.getId() == songId){
                index = queryVos.indexOf(queryVo);
            }
        }
        songList.setList(queryVos);
        model.addAttribute("type","songList");
        model.addAttribute("songList",songList);
        if(index == queryVos.size()-1){
            index = 0;
        }else{
            index += 1;
        }
        model.addAttribute("song", queryVos.get(index));
        return Result.resultBuilder().state(200).result(true).description("切换下一曲成功").data("song",queryVos.get(index));
    }

    @ApiOperation("点歌曲切歌")
    @GetMapping("/checkOneSong")
    public @ResponseBody Result checkOneSong(Integer songId, Model model,String token){
        User currentUser = tokenUtils.getCurrentUser1(token);
        int userId = currentUser.getId();
        String RedisSongList = redisTemplate.opsForValue().get("songList"+userId);
        SongListPlayerQueryVo songList = JSON.parseObject(RedisSongList, SongListPlayerQueryVo.class);
        List<SongPlayerQueryVo> queryVos = songList.getList();
        int index = 0;
        for (SongPlayerQueryVo queryVo : queryVos) {
            if(queryVo.getId() == songId){
                index = queryVos.indexOf(queryVo);
            }
        }
        songList.setList(queryVos);
        model.addAttribute("type","songList");
        model.addAttribute("songList",songList);
        model.addAttribute("song", queryVos.get(index));
        return Result.resultBuilder().state(200).result(true).description("切换下一曲成功").data("song",queryVos.get(index));
    }

    @ApiOperation("查前所有风格")
    @PostMapping("/findAllStyle")
    public @ResponseBody Result findAllStyle(){
        List<String> allListStyle = songListService.findAllDistinctStyle();
        if(!Objects.isNull(allListStyle)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("allListStyle",allListStyle);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("allListStyle", null);
        }
    }

    @ApiOperation("模糊查询歌单")
    @PostMapping("/fuzzyFind")
    public @ResponseBody Result fuzzyFind(String songName){
        List<SongList> songLists = songListService.fuzzyFind(songName);
        if(!Objects.isNull(songLists)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("list",songLists).data("name",songName).data("type","list");
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("list", null);
        }
    }

}
