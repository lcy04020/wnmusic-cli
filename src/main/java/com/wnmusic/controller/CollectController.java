package com.wnmusic.controller;


import com.wnmusic.common.Result;
import com.wnmusic.entity.User;
import com.wnmusic.queryvo.CollectQueryVo;
import com.wnmusic.service.CollectService;
import com.wnmusic.service.UserService;
import com.wnmusic.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Objects;


@Controller
@RequestMapping("/collect" )
public class CollectController {


    private CollectService collectService;

    private UserService userService;

    @Autowired
    public void setCollectService(CollectService collectService) {
        this.collectService = collectService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/person-center")
    public String personCenter(@RequestParam(value = "userName") String userName,Model model){
        model.addAttribute("userName",userName);
        return "person-center";
    }


    @RequestMapping("/findUser")
    public @ResponseBody Result findUserByUserName(@RequestParam(value = "userName") String userName, Model model){
        System.out.println("findUser 用户名:"+userName);
        User user = userService.findUserByUserName(userName);

        System.out.println("==================User:"+user.getLocation());

        if(!Objects.isNull(user)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("user",user);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("user", null);
        }

    }



    @PostMapping("/updateUser")
    public @ResponseBody Result updateUser(Integer id, @RequestParam(value = "userName") String userName,
                                           @RequestParam(value = "phoneNum") String phoneNum, @RequestParam(value = "gender") String gender,
                                           @RequestParam(value = "birth") String birth, @RequestParam(value = "location") String location, Model model){

        Date birthday = DateUtil.stringToDate(birth,"yyyy-MM-dd");
        User user = new User(id,userName,gender,phoneNum,birthday,location);
        Boolean f = userService.modifyUser(user);
        if(!Objects.isNull(user)){
            return Result.resultBuilder().state(200).result(f).description("修改成功").data("user",user);
        }else{
            return Result.resultBuilder().state(500).result(f).description("修改失败").data("user",null);
        }

    }



    /* 歌曲 */
    @GetMapping("/findAllCollect")
    public @ResponseBody Result findAllCollect(@RequestParam(value = "userName") String userName){
        System.out.println("===========================userName:"+userName);
        List<CollectQueryVo> list = collectService.selectAllCollect(userName);
        System.out.println("=============用户:"+list);


        if(!Objects.isNull(list)){

            return Result.resultBuilder().state(200).result(true).description("查找成功").data("user",list);

        }else{

            return Result.resultBuilder().state(500).result(false).description("查找失败").data("user", null);

        }
    }


    /* 专辑 */
    @GetMapping("/findSongListByName")
    public @ResponseBody Result findSongListByName(@RequestParam(value = "userName") String userName){
        List<CollectQueryVo> list = collectService.selectAllSongList(userName);

        if(!Objects.isNull(list)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("songList",list);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("songList",null);
        }

    }



    /* 专辑 */
    @GetMapping("/findSongAlbumByName")
    public @ResponseBody Result findSongAlbumByName(@RequestParam(value = "userName") String userName){
        List<CollectQueryVo> list = collectService.selectAllSongAlbum(userName);

        if(!Objects.isNull(list)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("songAlbum",list);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("songAlbum", null);
        }

    }

}
