package com.wnmusic.controller;

import com.wnmusic.common.Result;
import com.wnmusic.common.TokenUtils;
import com.wnmusic.entity.ListComment;
import com.wnmusic.entity.User;
import com.wnmusic.queryvo.ListCommentQueryVo;
import com.wnmusic.service.ListCommentService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.oas.annotations.EnableOpenApi;

import java.util.List;

@EnableOpenApi
@RestController
@ApiModel(value = "歌单评论管理")
@RequestMapping("/ListComment")
public class ListCommentController {

    private ListCommentService listCommentService;

    private TokenUtils tokenUtils;


    @Autowired
    public void setListCommentService(ListCommentService listCommentService) {
        this.listCommentService = listCommentService;
    }


    @Autowired
    public void setTokenUtils(TokenUtils tokenUtils) {
        this.tokenUtils = tokenUtils;
    }

    @ApiOperation("新增评论")
    @PostMapping("/addListComment")
    public Result addListComment(@RequestParam("userId") Integer userId,
                                 @RequestParam("listId") Integer listId,
                                 @RequestParam("content") String content,
                                 @RequestParam("parentId") Integer parentId,
                                 @RequestParam("token") String token
    ) {
        User currentUser1 = tokenUtils.getCurrentUser1(token);
        ListComment comment = new ListComment();
        comment.setUserId(currentUser1.getId());
        comment.setListId(listId);
        comment.setContent(content);
        comment.setParentId(parentId);
        if (parentId != null && parentId != 0) {
            int i = listCommentService.addChildComment(comment);
            if (i > 0) {
                return Result.resultBuilder().state(200).result(true).description("发表成功");
            } else {
                return Result.resultBuilder().state(500).result(false).description("发表失败");
            }
        } else {
            int i = listCommentService.add(comment);
            if (i > 0) {
                return Result.resultBuilder().state(200).result(true).description("发表成功");
            } else {
                return Result.resultBuilder().state(500).result(false).description("发表失败");
            }
        }
    }

    @ApiOperation("加载已有的歌曲评论（不包括子回复）")
    @PostMapping("/findAllListComment")
    @ResponseBody
    public Result findAllComment(@RequestParam("listId")Integer listId,
                                 @RequestParam("by")String by) {
        System.out.println(by+"排序");
        List<ListCommentQueryVo> allComment = listCommentService.findAllCommentByListId(listId, by);
        return Result.resultBuilder().state(200).data("allComment", allComment);
    }

    @ApiOperation("加载评论的子回复")
    @PostMapping("/findListCommentReply")
    @ResponseBody
    public Result loadListCommentReply(Integer parentId) {
        List<ListCommentQueryVo> commentReply = listCommentService.findListCommentReplyByParentId(parentId);
        return Result.resultBuilder().state(200).data("commentReply", commentReply);
    }


    @ApiOperation("给评论点赞/取消赞")
    @PostMapping("/commentUp")
    @ResponseBody
    public Result giveCommentUp(@RequestParam("userId") Integer userId,
                                @RequestParam("commentId") Integer commentId,
                                @RequestParam("token") String token) {
        User currentUser1 = tokenUtils.getCurrentUser1(token);
        boolean upAndCancel = listCommentService.upAndCancel(currentUser1.getId(), commentId);
        return Result.resultBuilder().state(200).data("upAndCancel", upAndCancel);
    }

    @ApiOperation("确认：你点赞了吗")
    @PostMapping("/upOrNot")
    @ResponseBody
    public Result upOrNot(@RequestParam("userId") Integer userId,
                          @RequestParam("commentId") Integer commentId,
                          @RequestParam("token") String token) {
        User currentUser1 = tokenUtils.getCurrentUser1(token);
        boolean upOrNot = listCommentService.upOrNot(currentUser1.getId(), commentId);
        return Result.resultBuilder().state(200).data("upOrNot", upOrNot);
    }

}
