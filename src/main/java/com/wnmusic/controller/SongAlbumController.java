package com.wnmusic.controller;


import com.alibaba.fastjson.JSON;
import com.wnmusic.common.Result;
import com.wnmusic.common.TokenUtils;
import com.wnmusic.entity.SongAlbum;
import com.wnmusic.entity.SongList;
import com.wnmusic.entity.User;
import com.wnmusic.queryvo.*;
import com.wnmusic.service.SongAlbumService;
import com.wnmusic.utils.GetRandomList;
import com.wnmusic.utils.GetSongTime;
import com.wnmusic.utils.SplitLyric;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.oas.annotations.EnableOpenApi;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;



@EnableOpenApi
@Controller
@ApiModel(value = "专辑管理")
@RequestMapping("/songAlbum")
public class SongAlbumController {
    private SongAlbumService songAlbumService;

    private StringRedisTemplate redisTemplate;

    private TokenUtils tokenUtils;

    @Autowired
    public void setSongAlbumService(SongAlbumService songAlbumService) {
        this.songAlbumService = songAlbumService;
    }

    @Autowired
    public void setRedisTemplate(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Autowired
    public void setTokenUtils(TokenUtils tokenUtils) {
        this.tokenUtils = tokenUtils;
    }

    @ApiOperation("查找全部")
    @GetMapping("/findAll")
    public @ResponseBody Result findAll(){
        List<SongAlbum> Album = songAlbumService.findAll();
        if(!Objects.isNull(Album)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("songAlbum",Album);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("songAlbum", null);
        }
    }

    @ApiOperation("根据风格查找")
    @PostMapping("/findByStyle")
    public @ResponseBody Result findByStyle(String style){
        List<SongAlbum> Album = songAlbumService.findByStyle(style);
        if(!Objects.isNull(Album)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("songAlbum",Album);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("songAlbum", null);
        }
    }

    @ApiOperation("查前15条")
    @GetMapping("/findFifteen")
    public @ResponseBody Result findFifteen(){
        List<SongAlbum> Album = songAlbumService.findFifteen();
        if(!Objects.isNull(Album)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("songAlbum",Album);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("songAlbum", null);
        }
    }

    @ApiOperation("随机查找15条")
    @GetMapping("/randomFind")
    public @ResponseBody Result randomFind(){
        //List<SongAlbum> Album = songAlbumService.randomFind();
        List<Integer> randomList = GetRandomList.getRandomList(15, 1, 100);
        List<SongAlbum> songAlbums = new ArrayList<>();
        for (int i = 0; i < randomList.size(); i++) {
            String songAlbum = Objects.requireNonNull(redisTemplate.opsForHash().get("allSongAlbum", randomList.get(i) + "")).toString();
            songAlbums.add(JSON.parseObject(songAlbum, SongAlbum.class));
        }
        if(!Objects.isNull(songAlbums)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("songAlbum",songAlbums);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("songAlbum", null);
        }
    }

    @ApiOperation("根据id查找个专辑歌曲，并跳转到播放页面")
    @GetMapping("/findByIdToPlayer")
    public String findByIdToPlayer(Integer id, Model model,String token){
        User currentUser = tokenUtils.getCurrentUser1(token);
        int userId = currentUser.getId();
        List<Integer> songIds = songAlbumService.findSongIdsById(id);
        SongAlbumPlayerQueryVo songAlbum = new SongAlbumPlayerQueryVo();
        List<SongPlayerQueryVo> songs = new ArrayList<>();
        for (Integer songId : songIds) {
            String s1 = redisTemplate.opsForValue().get(songId + "");
            String songTime = redisTemplate.opsForHash().get("allSongTime", songId + "").toString();
            SongPlayerQueryVo song = JSON.parseObject(s1, SongPlayerQueryVo.class);
            song.setSongTime(songTime);
            songs.add(song);
        }
        songAlbum.setList(songs);
        model.addAttribute("type","songList");
        model.addAttribute("songList",songAlbum);
        model.addAttribute("song", songs.get(0));
        redisTemplate.opsForValue().set("songList"+userId,JSON.toJSONString(songAlbum));
        return "songPlayer";
    }


    @ApiOperation("查前所有风格")
    @PostMapping("/findAllStyle")
    public @ResponseBody Result findAllStyle(){
        List<String> allAlbumStyle = songAlbumService.findAllDistinctStyle();
        if(!Objects.isNull(allAlbumStyle)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("allAlbumStyle",allAlbumStyle);
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("allAlbumStyle", null);
        }
    }

    @ApiOperation("模糊查询专辑")
    @PostMapping("/fuzzyFind")
    public @ResponseBody Result fuzzyFind(String songName){
        List<FindSongAlbumQueryVo> songAlbums = songAlbumService.fuzzyFind(songName);
        if(!Objects.isNull(songAlbums)){
            return Result.resultBuilder().state(200).result(true).description("查找成功").data("list",songAlbums).data("name",songName).data("type","list");
        }else{
            return Result.resultBuilder().state(500).result(false).description("查找失败").data("list", null);
        }
    }
}
