package com.wnmusic.controller;

import com.wnmusic.common.Result;
import com.wnmusic.common.TokenUtils;
import com.wnmusic.entity.User;
import com.wnmusic.entity.Vip;
import com.wnmusic.service.UserService;
import com.wnmusic.service.VipService;
import com.wnmusic.service.VipSongService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.oas.annotations.EnableOpenApi;

import java.util.Objects;

/**
 * @author lcy
 * @date 2022/9/9 11:18
 */
@EnableOpenApi
@Controller
@ApiModel(value = "vip管理")
@RequestMapping("/vip")
public class VipController {
    private VipService vipService;

    private VipSongService vipSongService;

    private UserService userService;

    private TokenUtils tokenUtils;

    @Autowired
    public void setVipService(VipService vipService) {
        this.vipService = vipService;
    }

    @Autowired
    public void setVipSongService(VipSongService vipSongService) {
        this.vipSongService = vipSongService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setTokenUtils(TokenUtils tokenUtils) {
        this.tokenUtils = tokenUtils;
    }

    @ApiOperation("判断用户是否是vip，决定歌曲是否能播放")
    @PostMapping("/checkVipToPlay")
    public @ResponseBody
    Result checkVipToPlay(Integer songId, String token) {
        Integer songVipStatus = vipSongService.findStatusBySongId(songId);
        if (songVipStatus == 1) {
            return Result.resultBuilder().state(200).result(true).description("非vip歌曲可以直接播放");
        } else {
//            System.out.println(token);
            User currentUser = tokenUtils.getCurrentUser1(token);
            System.out.println("--------------+++++++++" + currentUser);
//            System.out.println("--------------+++++++++"+username);
            if (Objects.isNull(currentUser)) {
                return Result.resultBuilder().state(500).result(false).description("未登录，无法播放vip歌曲");
            }
            //User user = userService.findUserByUserName(username);
            Integer userId = currentUser.getId();
            Integer userVipStatus = vipService.findStatusByUserId(userId);
            if (Objects.isNull(userVipStatus) || userVipStatus == 1) {
                return Result.resultBuilder().state(500).result(false).description("非vip用户，无法播放");
            } else {
                return Result.resultBuilder().state(200).result(true).description("vip用户可以播放vip歌曲");
            }
        }
    }


    @ApiOperation("判断用户是否是vip")
    @PostMapping("/checkVip")
    @ResponseBody
    public Result checkVip(@RequestParam("Authorization") String token) {

        User currentUser = tokenUtils.getCurrentUser1(token);
        Integer userId = currentUser.getId();

        Integer userVipStatus = vipService.findStatusByUserId(userId);
        if (Objects.isNull(userVipStatus) || userVipStatus == 1) {
            return Result.resultBuilder().state(500).result(false).description("非vip用户,请开通").data("userVipStatus",
                    userVipStatus);
        } else {
            return Result.resultBuilder().state(200).result(true).description("尊敬的vip客服你好").data("userVipStatus",
                    userVipStatus);
        }
    }

    @ApiOperation("开通vip")
    @PostMapping("/openVip")
    @ResponseBody
    public Result openVip(@RequestParam("Authorization") String token,@RequestParam("money") String money) {

        User currentUser = tokenUtils.getCurrentUser1(token);
        Integer userId = currentUser.getId();
        Integer month = null;
        double money1 = Double.parseDouble(money);

        if(money1 == 15){
            month = 1;
        }else if(money1 == 45){
            month = 3;
        }else if(money1 == 90){
            month = 6;
        }else if(money1 == 168){
            month = 12;
        }

        Vip vip = vipService.findVipByUserId(userId);

        int i = 0;
        if(Objects.isNull(vip)){
            i = vipService.addVip(new Vip(0, userId, month));
        }else{
            i = vipService.modifyVipByUserId(new Vip(0, userId, month));
        }

        if(i>0){
            return Result.resultBuilder().state(200).result(true).description("开通了"+month+"月的vip");
        }else{
            return Result.resultBuilder().state(500).result(false).description("开通失败");
        }

    }
}
