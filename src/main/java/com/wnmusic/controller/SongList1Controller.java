package com.wnmusic.controller;


import com.wnmusic.queryvo.ListDetailVo;
import com.wnmusic.service.SongListService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import springfox.documentation.oas.annotations.EnableOpenApi;


@EnableOpenApi
@Controller
@ApiModel(value = "专辑管理")
@RequestMapping("/songList")
public class SongList1Controller {


    private SongListService songListService;

    @Autowired
    public void setSongListService(SongListService songListService) {
        this.songListService = songListService;
    }


    @ApiOperation("根据主键查找专辑")
    @RequestMapping("/findListByKey")
    public String findListByKey(@RequestParam("id") Integer id, Model model) {
        ListDetailVo listDetailVo = songListService.findListDetailVoByKey(id);
        model.addAttribute("listDetailVo", listDetailVo);
        return "listDetail";
    }
}
