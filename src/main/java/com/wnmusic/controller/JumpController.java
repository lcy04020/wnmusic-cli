package com.wnmusic.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class JumpController {

    @RequestMapping("/jump")
    public String jump(String url){
        System.out.println();
        return url;
    }
}
