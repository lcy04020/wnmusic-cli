package com.wnmusic.controller;

import com.alipay.easysdk.factory.Factory;
import com.wnmusic.service.AlipayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Controller
@CrossOrigin
@RequestMapping("/pay")
public class AlipayController {

    @Autowired
    private AlipayService alipayService;


    @GetMapping("/topay")
    @ResponseBody
    public String toPay(String subject, BigDecimal money) throws Exception {
        String form = alipayService.toPay(subject, money);
        System.out.println(form);
        return form;
    }

    @PostMapping("/callback")
    @ResponseBody
    public String notifyCallback(HttpServletRequest request) throws Exception {
        System.out.println("进入异步");
        String success = "success";
        String failure = "failure";

        // https://opendocs.alipay.com/open/54/00y8k9 新老版本说明中有异步通知的新版说明
        // 获取支付宝异步回调信息, 将其转为 Map<String, String>
        Map<String, String> params = new HashMap<>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            // 乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
            // valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
            params.put(name, valueStr);
        }

        // 新版 SDK 不用移除 sign_type
//         params.remove("sign_type");

        // 验签
        boolean signVerified = Factory.Payment.Common().verifyNotify(params);

        if (signVerified) { // 验签通过
            System.out.println("通过验签");
            return success;
        } else { // 验签失败
            return failure;
        }
    }

    @GetMapping("/query")
    public String queryTradeStatus(@RequestParam("out_trade_no") String outTradeNo, Model model) throws Exception {
        System.out.println("test订单" + outTradeNo);
        Object result = alipayService.queryTradeStatus(outTradeNo);
        System.out.println(result);

        String[] split = result.toString().split(",");
        String s = split[split.length - 1];//状态
        String s22 = split[split.length -3];//价格


        String[] split1 = s.split("=");
        String s1 = split1[split1.length - 1];

        String[] split2 = s22.split("=");
        String s33 = split2[split2.length - 1];

        System.out.println("s33"+s33);
        String money = s33.substring(0, s33.length());

        String status = s1.substring(0, s1.length() - 1);
        System.out.println("状态"+status);
        System.out.println("价格"+money);


        model.addAttribute("tradeStatus", status);
        model.addAttribute("money",money);

        return "index";
    }


}
