package com.wnmusic.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;



/**
 * 跨域请求放行
 */
@Configuration
public class CorsConfig {
    /**
     * 跨域
     * @return
     */
    @Bean
    public CorsFilter corsFilter() {
        final UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration corsConfiguration = new CorsConfiguration();
        //-- 运行跨域时 带上cookie
        corsConfiguration.setAllowCredentials(true);
        //-- 这三个才是重点
        //-- 允许任意域名
        corsConfiguration.addAllowedOrigin("*");
        //-- 允许任意请求头  主要是我们后期要放token
        corsConfiguration.addAllowedHeader("*");
        //-- 允许任意请求方法！
        corsConfiguration.addAllowedMethod("*");
        //-- 任意的请求来源！
        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
        return new CorsFilter(urlBasedCorsConfigurationSource);
    }
}

