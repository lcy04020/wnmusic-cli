package com.wnmusic.config;

import cn.hutool.core.util.StrUtil;
import com.wnmusic.common.Constants;
import com.wnmusic.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author syy
 * @date 2022/09/03
 **/
public class JwtInterceptor implements HandlerInterceptor {

    private Logger logger = LoggerFactory.getLogger(JwtInterceptor.class);


    @Autowired
    private StringRedisTemplate redisTemplate;

//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
//        String token = request.getHeader("Authorization");
//        logger.debug(token);
//        // 如果不是映射到方法直接通过
//        if (!(handler instanceof HandlerMethod)) {
//            return true;
//        }
//        // 执行认证 判断从请求中获取到的Token是否合法如果不存在的话，token为null 我们直接进行业务处理会空指针
//        if (token == null) {
//            response.sendRedirect("/wnmusic");
//            throw new ServiceException(Constants.CODE_401, "无token，请重新登录");
//        }
//
//        //-- Controller 登录成功后 以 Token 作为键 拿 Token 作为值，放到Redis中
//        String redisToken = redisTemplate.opsForValue().get(token);
//
//        if (StrUtil.isBlank(redisToken)) {
//            response.sendRedirect("/wnmusic");
//            throw new ServiceException(Constants.CODE_401, "token验证失败，请重新登录");
//        }
//        return true;
//
//    }

}