package com.wnmusic.config;

import com.alipay.easysdk.factory.Factory;
import com.alipay.easysdk.kernel.Config;
import com.wnmusic.utils.FileUtils;
import org.springframework.stereotype.Component;

@Component
public class AlipayConfig {

    // 1. 设置参数（全局只需设置一次）
    static {
        Factory.setOptions(getOptions());
    }

    private static Config getOptions() {
        Config config = new Config();

        config.protocol = "https";

        // 沙箱环境修改为 openapi.alipaydev.com
        config.gatewayHost = "openapi.alipaydev.com";

        config.signType = "RSA2";

        config.appId = "2021000121608594";

        // 为避免私钥随源码泄露，推荐从文件中读取私钥字符串而不是写入源码中
        config.merchantPrivateKey = FileUtils.readFileOfTxt("C:\\Users\\小可爱\\Documents\\支付宝开放平台开发助手\\RSA密钥\\私钥.txt");

        //注：如果采用非证书模式，则无需赋值上面的三个证书路径，改为赋值如下的支付宝公钥字符串即可
        config.alipayPublicKey = "        " +
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAleqLgrs0zvUmBELzuKlyjWsSAOFMN6xA/S6HHtX9ppgxb/OKyoXEyrqYN13YeT23Y8RRiiNbBfwsuH7nDdhemHqhykbIfVPUV+0bHfED435q9adte3ldHeJpjDSqMbLdAibQRwmqw+6hKuQ84p0JeIWD6SJfvYGl2XPVwnHoGZjH1LxE3O1epHdGPgG3HP7CirULbWIgn8IjwiqhOwFnlDAzOA7Eol1vH3zaLRPmRFmsY8hZroqPv3+uHiJHgJLCgdy+dLz0wzJE2+bue6sUucVGrNkmeRhh5xPf4gvFnF7lu7MG2jpeFqWN0BBn9+Mdx5C64qKBZOPDPipkIAGaCQIDAQAB";
//        MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA
        //可设置异步通知接收服务地址（可选）（该地址需要外网能够访问）
        config.notifyUrl = "http://localhost:8888/wnmusic/pay/callback";

        return config;
    }
}
