package com.wnmusic.common;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.wnmusic.entity.User;
import com.wnmusic.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @author syy
 * @date 2022/09/03
 **/
@Component
public class TokenUtils {

    /**
     * PlayLoad 中有用户的信息，需要通过Service获取，所以要注入Service
     */
    private UserService userService;

    private Logger logger = LoggerFactory.getLogger(TokenUtils.class);

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public UserService getUserService() {
        return userService;
    }



    /**
     * 生成token
     *
     * @param userId    用户编号
     * @return
     */
    public String genToken(String userId,String userPassword) {
        return JWT.create().withAudience(userId) // 将 user id 保存到 token 里面,作为载荷
                .withExpiresAt(DateUtil.offsetHour(new Date(), 2)) // 2小时后token过期
                .sign(Algorithm.HMAC256(userPassword)); // 以 secretKey + password 作为 token 的密钥 secret
    }

    /**
     * 获取当前登录的用户信息
     *
     * @return user对象
     */
    public User getCurrentUser() {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            String token = request.getHeader("Authorization");
            System.out.println(token);
            //-- StrUtil HuTool中关于String的工具方法！
            if (StrUtil.isNotBlank(token)) {
                String userId = JWT.decode(token).getAudience().get(0);
                return userService.findUserByUserName(userId);
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }

    public User getCurrentUser1(String token) {
        System.out.println(token);
        if (StrUtil.isNotBlank(token)) {
            String userId = JWT.decode(token).getAudience().get(0);
            return userService.findUserByPrimaryKey(Integer.valueOf(userId));
        }
        return null;
    }
}
