package com.wnmusic.common;

import java.util.HashMap;
import java.util.Map;

public class Result {
    private Integer state;
    private Boolean result;
    private String description;
    private Map<String, Object> data = new HashMap<>();

    private Result() {

    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public static Result resultBuilder(){
        return new Result();
    }

    public Result state(int state){
        this.state = state;
        return this;
    }

    public Result result(boolean result){
        this.result = result;
        return this;
    }

    public Result description(String description){
        this.description = description;
        return this;
    }

    public Result data(String key,Object value){
        this.data.put(key,value);
        return this;
    }

    public Result data(Map<String,Object> map){
        this.data = map;
        return this;
    }

    @Override
    public String toString() {
        return "Result{" +
                "state=" + state +
                ", result=" + result +
                ", description='" + description + '\'' +
                ", data=" + data +
                '}';
    }
}
