package com.wnmusic.queryvo;

public class SongAlbumRankQueryVo {
    private Integer songAlbumId;
    private String title;
    private double songAlbumAvg;

    public SongAlbumRankQueryVo() {
    }

    public SongAlbumRankQueryVo(Integer songAlbumId, String title, double SongAlbumAvg) {
        this.songAlbumId = songAlbumId;
        this.title = title;
        this.songAlbumAvg = SongAlbumAvg;
    }

    public Integer getSongAlbumId() {
        return songAlbumId;
    }

    public void setSongAlbumId(Integer songAlbumId) {
        this.songAlbumId = songAlbumId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getSongAlbumAvg() {
        return songAlbumAvg;
    }

    public void setSongAlbumAvg(double songAlbumAvg) {
        this.songAlbumAvg = songAlbumAvg;
    }

    @Override
    public String toString() {
        return "SongAlbumRankQueryVo{" +
                "SongAlbumId=" + songAlbumId +
                ", title='" + title + '\'' +
                ", SongAlbumAvg=" + songAlbumAvg +
                '}';
    }
}
