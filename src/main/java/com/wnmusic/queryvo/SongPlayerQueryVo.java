package com.wnmusic.queryvo;

import java.util.Arrays;

public class SongPlayerQueryVo {
    private Integer id;
    private String songName;
    private String singerName;
    private String lyric;
    private String url;
    private String pic;
    private String songTime;
    private String[][] songLyric;

    public SongPlayerQueryVo() {
    }

    public SongPlayerQueryVo(Integer id, String songName, String singerName, String lyric, String url, String pic, String songTime, String[][] songLyric) {
        this.id = id;
        this.songName = songName;
        this.singerName = singerName;
        this.lyric = lyric;
        this.url = url;
        this.pic = pic;
        this.songTime = songTime;
        this.songLyric = songLyric;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getSingerName() {
        return singerName;
    }

    public void setSingerName(String singerName) {
        this.singerName = singerName;
    }

    public String getLyric() {
        return lyric;
    }

    public void setLyric(String lyric) {
        this.lyric = lyric;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSongTime() {
        return songTime;
    }

    public void setSongTime(String songTime) {
        this.songTime = songTime;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String[][] getSongLyric() {
        return songLyric;
    }

    public void setSongLyric(String[][] songLyric) {
        this.songLyric = songLyric;
    }

    @Override
    public String toString() {
        return "SongPlayerQueryVo{" +
                "id=" + id +
                ", songName='" + songName + '\'' +
                ", singerName='" + singerName + '\'' +
                ", lyric='" + lyric + '\'' +
                ", url='" + url + '\'' +
                ", pic='" + pic + '\'' +
                ", songTime='" + songTime + '\'' +
                ", songLyric=" + Arrays.toString(songLyric) +
                '}';
    }
}
