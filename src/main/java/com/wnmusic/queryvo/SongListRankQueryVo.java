package com.wnmusic.queryvo;

public class SongListRankQueryVo {
    private Integer songListId;
    private String title;
    private double songListAvg;

    public SongListRankQueryVo() {
    }

    public SongListRankQueryVo(Integer songListId, String title, double songListAvg) {
        this.songListId = songListId;
        this.title = title;
        this.songListAvg = songListAvg;
    }

    public Integer getSongListId() {
        return songListId;
    }

    public void setSongListId(Integer songListId) {
        this.songListId = songListId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getSongListAvg() {
        return songListAvg;
    }

    public void setSongListAvg(double songListAvg) {
        this.songListAvg = songListAvg;
    }

    @Override
    public String toString() {
        return "SongListRankQueryVo{" +
                "SongListId=" + songListId +
                ", title='" + title + '\'' +
                ", SongListAvg=" + songListAvg +
                '}';
    }
}
