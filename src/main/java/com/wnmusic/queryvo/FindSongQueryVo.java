package com.wnmusic.queryvo;

/**
 * @author lcy
 * @date 2022/9/7 14:38
 */
public class FindSongQueryVo {
    private Integer id;
    private String songName;
    private String singerName;
    private Integer albumId;
    private String title;

    public FindSongQueryVo() {
    }

    public FindSongQueryVo(Integer id, String songName, String singerName, Integer albumId, String title) {
        this.id = id;
        this.songName = songName;
        this.singerName = singerName;
        this.albumId = albumId;
        this.title = title;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getSingerName() {
        return singerName;
    }

    public void setSingerName(String singerName) {
        this.singerName = singerName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getAlbumId() {
        return albumId;
    }

    public void setAlbumId(Integer albumId) {
        this.albumId = albumId;
    }

    @Override
    public String toString() {
        return "FindSongQueryVo{" +
                "id=" + id +
                ", songName='" + songName + '\'' +
                ", singerName='" + singerName + '\'' +
                ", albumId=" + albumId +
                ", title='" + title + '\'' +
                '}';
    }
}
