package com.wnmusic.queryvo;


import com.wnmusic.entity.SongAlbum;
import java.util.List;

public class AlbumDetailVo {

    private SongAlbum songAlbum;
    private Integer singerId;
    private String singerName;
    private List<SongDetailVo> songsList;


    public AlbumDetailVo() {
    }

    public SongAlbum getSongAlbum() {
        return songAlbum;
    }

    public void setSongAlbum(SongAlbum songAlbum) {
        this.songAlbum = songAlbum;
    }

    public Integer getSingerId() {
        return singerId;
    }

    public void setSingerId(Integer singerId) {
        this.singerId = singerId;
    }

    public String getSingerName() {
        return singerName;
    }

    public void setSingerName(String singerName) {
        this.singerName = singerName;
    }

    public List<SongDetailVo> getSongsList() {
        return songsList;
    }

    public void setSongsList(List<SongDetailVo> songsList) {
        this.songsList = songsList;
    }

    @Override
    public String toString() {
        return "AlbumDetailVo{" +
                "songAlbum=" + songAlbum +
                ", singerId=" + singerId +
                ", singerName='" + singerName + '\'' +
                ", songsList=" + songsList +
                '}';
    }
}
