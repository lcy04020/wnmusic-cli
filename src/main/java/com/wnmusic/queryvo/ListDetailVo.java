package com.wnmusic.queryvo;


import com.wnmusic.entity.SongList;

import java.util.List;

public class ListDetailVo {

    private SongList songList;
    private List<SongDetailVo> songsList;


    public ListDetailVo() {
    }

    public SongList getSongList() {
        return songList;
    }

    public void setSongList(SongList songList) {
        this.songList = songList;
    }

    public List<SongDetailVo> getSongsList() {
        return songsList;
    }

    public void setSongsList(List<SongDetailVo> songsList) {
        this.songsList = songsList;
    }

    @Override
    public String toString() {
        return "ListDetailVo{" +
                "songList=" + songList +
                ", songsList=" + songsList +
                '}';
    }
}
