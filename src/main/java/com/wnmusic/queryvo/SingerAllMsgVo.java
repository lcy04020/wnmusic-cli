package com.wnmusic.queryvo;

/**
 * @author syy
 * @date 2022/09/08
 **/
public class SingerAllMsgVo {
    private Integer songId;
    /**
     * 歌手的
     */
    private String singerName;
    private String singerPic;
    private String introduction;

    /**
     * 歌曲的
     */
    private String songName;
    private String songPic;

    /**
     * 专辑的
     */
    private Integer songAlbumId;
    private String title;
    private String songAlbumPic;

    public SingerAllMsgVo(Integer songId, String singerName, String singerPic, String introduction, String songName, String songPic, Integer songAlbumId, String title, String songAlbumPic) {
        this.songId = songId;
        this.singerName = singerName;
        this.singerPic = singerPic;
        this.introduction = introduction;
        this.songName = songName;
        this.songPic = songPic;
        this.songAlbumId = songAlbumId;
        this.title = title;
        this.songAlbumPic = songAlbumPic;
    }

    public SingerAllMsgVo() {
    }

    public Integer getSongId() {
        return songId;
    }

    public void setSongId(Integer songId) {
        this.songId = songId;
    }

    public String getSingerName() {
        return singerName;
    }

    public void setSingerName(String singerName) {
        this.singerName = singerName;
    }

    public String getSingerPic() {
        return singerPic;
    }

    public void setSingerPic(String singerPic) {
        this.singerPic = singerPic;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getSongPic() {
        return songPic;
    }

    public void setSongPic(String songPic) {
        this.songPic = songPic;
    }

    public Integer getSongAlbumId() {
        return songAlbumId;
    }

    public void setSongAlbumId(Integer songAlbumId) {
        this.songAlbumId = songAlbumId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSongAlbumPic() {
        return songAlbumPic;
    }

    public void setSongAlbumPic(String songAlbumPic) {
        this.songAlbumPic = songAlbumPic;
    }

    @Override
    public String toString() {
        return "SingerAllMsgVo{" +
                "songId=" + songId +
                ", singerName='" + singerName + '\'' +
                ", singerPic='" + singerPic + '\'' +
                ", introduction='" + introduction + '\'' +
                ", songName='" + songName + '\'' +
                ", songPic='" + songPic + '\'' +
                ", songAlbumId=" + songAlbumId +
                ", title='" + title + '\'' +
                ", songAlbumPic='" + songAlbumPic + '\'' +
                '}';
    }
}
