package com.wnmusic.queryvo;

import java.util.List;

public class SongListPlayerQueryVo {
    private Integer id;
    private String title;
    private List<SongPlayerQueryVo> list;

    public SongListPlayerQueryVo() {
    }

    public SongListPlayerQueryVo(Integer id, String title, List<SongPlayerQueryVo> list) {
        this.id = id;
        this.title = title;
        this.list = list;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<SongPlayerQueryVo> getList() {
        return list;
    }

    public void setList(List<SongPlayerQueryVo> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "SongListPlayerQueryVo{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", list=" + list +
                '}';
    }
}
