package com.wnmusic.queryvo;

import com.wnmusic.entity.Song;
import com.wnmusic.entity.SongAlbum;
import com.wnmusic.entity.SongList;
import com.wnmusic.entity.User;

import java.util.Date;

public class CollectQueryVo {


    private Integer id;

    private User user;

    private Integer type;

    private Song song;

    private SongList songList;

    private Integer songListId;

    private SongAlbum songAlbum;

    private Date createTime;

    public CollectQueryVo() {
    }

    public CollectQueryVo(User user, Integer type, Song song, SongList songList, Integer songListId, SongAlbum songAlbum, Date createTime) {
        this.user = user;
        this.type = type;
        this.song = song;
        this.songList = songList;
        this.songListId = songListId;
        this.songAlbum = songAlbum;
        this.createTime = createTime;
    }

    public CollectQueryVo(Integer id, User user, Integer type, Song song, SongList songList, Integer songListId, SongAlbum songAlbum, Date createTime) {
        this.id = id;
        this.user = user;
        this.type = type;
        this.song = song;
        this.songList = songList;
        this.songListId = songListId;
        this.songAlbum = songAlbum;
        this.createTime = createTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Song getSong() {
        return song;
    }

    public void setSong(Song song) {
        this.song = song;
    }

    public SongList getSongList() {
        return songList;
    }

    public void setSongList(SongList songList) {
        this.songList = songList;
    }

    public Integer getSongListId() {
        return songListId;
    }

    public void setSongListId(Integer songListId) {
        this.songListId = songListId;
    }

    public SongAlbum getSongAlbum() {
        return songAlbum;
    }

    public void setSongAlbum(SongAlbum songAlbum) {
        this.songAlbum = songAlbum;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "CollectQueryVo{" +
                "id=" + id +
                ", user=" + user +
                ", type=" + type +
                ", song=" + song +
                ", songList=" + songList +
                ", songListId=" + songListId +
                ", songAlbum=" + songAlbum +
                ", createTime=" + createTime +
                '}';
    }
}
