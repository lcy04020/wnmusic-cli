package com.wnmusic.queryvo;

import com.wnmusic.entity.Singer;
import com.wnmusic.entity.Song;
import com.wnmusic.entity.SongAlbum;

import java.io.Serializable;

public class AlbumSongQueryVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Singer singer;

    private Song song;

    private SongAlbum songAlbum;

    public AlbumSongQueryVo() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Singer getSinger() {
        return singer;
    }

    public void setSinger(Singer singer) {
        this.singer = singer;
    }

    public Song getSong() {
        return song;
    }

    public void setSong(Song song) {
        this.song = song;
    }

    public SongAlbum getSongAlbum() {
        return songAlbum;
    }

    public void setSongAlbum(SongAlbum songAlbum) {
        this.songAlbum = songAlbum;
    }

    public AlbumSongQueryVo(Singer singer, Song song, SongAlbum songAlbum) {
        this.singer = singer;
        this.song = song;
        this.songAlbum = songAlbum;
    }

    public AlbumSongQueryVo(Integer id, Singer singer, Song song, SongAlbum songAlbum) {
        this.id = id;
        this.singer = singer;
        this.song = song;
        this.songAlbum = songAlbum;
    }

    @Override
    public String toString() {
        return "AlbumSongQueryVo{" +
                "id=" + id +
                ", singer=" + singer +
                ", song=" + song +
                ", songAlbum=" + songAlbum +
                '}';
    }
}
