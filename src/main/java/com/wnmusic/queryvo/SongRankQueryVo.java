package com.wnmusic.queryvo;

public class SongRankQueryVo {
    private Integer songId;
    private String name;
    private double songAvg;

    public SongRankQueryVo() {
    }

    public SongRankQueryVo(Integer songId, String name, double songAvg) {
        this.songId = songId;
        this.name = name;
        this.songAvg = songAvg;
    }

    public Integer getSongId() {
        return songId;
    }

    public void setSongId(Integer songId) {
        this.songId = songId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSongAvg() {
        return songAvg;
    }

    public void setSongAvg(double songAvg) {
        this.songAvg = songAvg;
    }

    @Override
    public String toString() {
        return "SongRankQueryVo{" +
                "songId=" + songId +
                ", name='" + name + '\'' +
                ", songAvg=" + songAvg +
                '}';
    }
}
