package com.wnmusic.queryvo;

import java.util.Date;

public class SongDetailVo {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 歌手id
     */
    private Integer singerId;

    /**
     * 歌手姓名
     */
    private String singerName;

    /**
     * 歌手性别
     */
    private String singerGender;

    /**
     * 歌手地区
     */
    private String singerLocation;

    /**
     * 歌名
     */
    private String name;

    /**
     * 歌曲图片
     */
    private String pic;

    /**
     * 歌词
     */
    private String lyric;

    /**
     * 歌曲地址
     */
    private String url;


   public SongDetailVo() {
   }

   public Integer getId() {
      return id;
   }

   public void setId(Integer id) {
      this.id = id;
   }

   public Integer getSingerId() {
      return singerId;
   }

   public void setSingerId(Integer singerId) {
      this.singerId = singerId;
   }

   public String getSingerName() {
      return singerName;
   }

   public void setSingerName(String singerName) {
      this.singerName = singerName;
   }

   public String getSingerGender() {
      return singerGender;
   }

   public void setSingerGender(String singerGender) {
      this.singerGender = singerGender;
   }

   public String getSingerLocation() {
      return singerLocation;
   }

   public void setSingerLocation(String singerLocation) {
      this.singerLocation = singerLocation;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getPic() {
      return pic;
   }

   public void setPic(String pic) {
      this.pic = pic;
   }

   public String getLyric() {
      return lyric;
   }

   public void setLyric(String lyric) {
      this.lyric = lyric;
   }

   public String getUrl() {
      return url;
   }

   public void setUrl(String url) {
      this.url = url;
   }

   @Override
   public String toString() {
      return "SongDetailVo{" +
              "id=" + id +
              ", singerId=" + singerId +
              ", singerName='" + singerName + '\'' +
              ", singerGender='" + singerGender + '\'' +
              ", singerLocation='" + singerLocation + '\'' +
              ", name='" + name + '\'' +
              '}';
   }
}
