package com.wnmusic.queryvo;

import com.wnmusic.entity.Singer;

public class SongQueryVo {

    private Integer id;

    private String name;

    private Singer singer;

    public SongQueryVo() {
    }

    public SongQueryVo(String name, Singer singer) {
        this.name = name;
        this.singer = singer;
    }

    public SongQueryVo(Integer id, String name, Singer singer) {
        this.id = id;
        this.name = name;
        this.singer = singer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Singer getSinger() {
        return singer;
    }

    public void setSinger(Singer singer) {
        this.singer = singer;
    }

    @Override
    public String toString() {
        return "SongQueryVo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", singer=" + singer +
                '}';
    }
}
