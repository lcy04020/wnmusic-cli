package com.wnmusic.queryvo;

import java.util.Date;

/**
 * @author lcy
 * @date 2022/9/7 18:55
 */
public class FindSongAlbumQueryVo {
    private Integer albumId;
    private String title;
    private String pic;
    private Integer singerId;
    private String singerName;

    public FindSongAlbumQueryVo() {
    }

    public FindSongAlbumQueryVo(Integer albumId, String title, String pic, Integer singerId, String singerName) {
        this.albumId = albumId;
        this.title = title;
        this.pic = pic;
        this.singerId = singerId;
        this.singerName = singerName;
    }

    public Integer getAlbumId() {
        return albumId;
    }

    public void setAlbumId(Integer albumId) {
        this.albumId = albumId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getSingerName() {
        return singerName;
    }

    public void setSingerName(String singerName) {
        this.singerName = singerName;
    }

    public Integer getSingerId() {
        return singerId;
    }

    public void setSingerId(Integer singerId) {
        this.singerId = singerId;
    }

    @Override
    public String toString() {
        return "FindSongAlbumQueryVo{" +
                "albumId=" + albumId +
                ", title='" + title + '\'' +
                ", pic='" + pic + '\'' +
                ", singerId=" + singerId +
                ", singerName='" + singerName + '\'' +
                '}';
    }
}
