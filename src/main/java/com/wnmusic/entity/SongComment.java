package com.wnmusic.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * 评论
 * @TableName song_comment
 */
public class SongComment implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 歌曲id
     */
    private Integer songId;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 评论时间
     */
    private Date createTime;

    /**
     * 点赞数
     */
    private Integer up;

    /**
     * 评论数
     */
    private Integer reply;

    /**
     * 父级
     */
    private Integer parentId;

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 用户id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 用户id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 歌曲id
     */
    public Integer getSongId() {
        return songId;
    }

    /**
     * 歌曲id
     */
    public void setSongId(Integer songId) {
        this.songId = songId;
    }

    /**
     * 评论内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 评论内容
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 评论时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 评论时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 评论点赞数
     */
    public Integer getUp() {
        return up;
    }

    /**
     * 评论点赞数
     */
    public void setUp(Integer up) {
        this.up = up;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getReply() {
        return reply;
    }

    public void setReply(Integer reply) {
        this.reply = reply;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SongComment that = (SongComment) o;
        return Objects.equals(id, that.id) && Objects.equals(userId, that.userId) && Objects.equals(songId, that.songId) && Objects.equals(content, that.content) && Objects.equals(createTime, that.createTime) && Objects.equals(up, that.up) && Objects.equals(parentId, that.parentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, songId, content, createTime, up, parentId);
    }

    @Override
    public String toString() {
        return "SongComment{" +
                "id=" + id +
                ", userId=" + userId +
                ", songId=" + songId +
                ", content='" + content + '\'' +
                ", createTime=" + createTime +
                ", up=" + up +
                ", reply=" + reply +
                ", parentId=" + parentId +
                '}';
    }
}