package com.wnmusic.entity;

import java.io.Serializable;
import java.util.Date;

/**
* 收藏
* @TableName collect
*/
public class Collect implements Serializable {

    /**
    * 主键
    */
    private Integer id;

    /**
    * 用户id
    */
    private Integer userId;

    /**
    * 收藏类型（0歌曲1歌单2专辑）
    */
    private Integer type;

    /**
    * 歌曲id
    */
    private Integer songId;

    /**
    * 歌单id
    */
    private Integer songListId;

    /**
    * 专辑id
    */
    private Integer songAlbumId;

    /**
    * 收藏时间
    */
    private Date createTime;

    /**
    * 主键
    */
    private void setId(Integer id){
    this.id = id;
    }

    /**
    * 用户id
    */
    private void setUserId(Integer userId){
    this.userId = userId;
    }

    /**
    * 收藏类型（0歌曲1歌单2专辑）
    */
    private void setType(Integer type){
    this.type = type;
    }

    /**
    * 歌曲id
    */
    private void setSongId(Integer songId){
    this.songId = songId;
    }

    /**
    * 歌单id
    */
    private void setSongListId(Integer songListId){
    this.songListId = songListId;
    }

    /**
    * 专辑id
    */
    private void setSongAlbumId(Integer songAlbumId){
    this.songAlbumId = songAlbumId;
    }

    /**
    * 收藏时间
    */
    private void setCreateTime(Date createTime){
    this.createTime = createTime;
    }


    /**
    * 主键
    */
    private Integer getId(){
    return this.id;
    }

    /**
    * 用户id
    */
    private Integer getUserId(){
    return this.userId;
    }

    /**
    * 收藏类型（0歌曲1歌单2专辑）
    */
    private Integer getType(){
    return this.type;
    }

    /**
    * 歌曲id
    */
    private Integer getSongId(){
    return this.songId;
    }

    /**
    * 歌单id
    */
    private Integer getSongListId(){
    return this.songListId;
    }

    /**
    * 专辑id
    */
    private Integer getSongAlbumId(){
    return this.songAlbumId;
    }

    /**
    * 收藏时间
    */
    private Date getCreateTime(){
    return this.createTime;
    }

    public Collect() {
    }

    public Collect(Integer userId, Integer type, Integer songId, Integer songListId, Integer songAlbumId, Date createTime) {
        this.userId = userId;
        this.type = type;
        this.songId = songId;
        this.songListId = songListId;
        this.songAlbumId = songAlbumId;
        this.createTime = createTime;
    }

    public Collect(Integer id, Integer userId, Integer type, Integer songId, Integer songListId, Integer songAlbumId, Date createTime) {
        this.id = id;
        this.userId = userId;
        this.type = type;
        this.songId = songId;
        this.songListId = songListId;
        this.songAlbumId = songAlbumId;
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "Collect{" +
                "id=" + id +
                ", userId=" + userId +
                ", type=" + type +
                ", songId=" + songId +
                ", songListId=" + songListId +
                ", songAlbumId=" + songAlbumId +
                ", createTime=" + createTime +
                '}';
    }
}
