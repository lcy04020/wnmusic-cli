package com.wnmusic.entity;

import java.io.Serializable;

/**
 * 
 * @TableName vip_song
 */
public class VipSong implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 歌曲id
     */
    private Integer songId;

    /**
     * 0为vip专属1为免费
     */
    private Integer status;

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 歌曲id
     */
    public Integer getSongId() {
        return songId;
    }

    /**
     * 歌曲id
     */
    public void setSongId(Integer songId) {
        this.songId = songId;
    }

    /**
     * 0为vip专属1为免费
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 0为vip专属1为免费
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        VipSong other = (VipSong) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getSongId() == null ? other.getSongId() == null : this.getSongId().equals(other.getSongId()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getSongId() == null) ? 0 : getSongId().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", songId=").append(songId);
        sb.append(", status=").append(status);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}