package com.wnmusic.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @TableName vip
 */
public class Vip implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 用户的id
     */
    private Integer userId;

    /**
     * 0代表为vip 1代表过期
     */
    private Integer status;

    /**
     * vip开通时间
     */
    private Date createTime;

    /**
     * 开通月数
     */
    private Integer month;

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 用户的id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 用户的id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 0代表为vip 1代表过期
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 0代表为vip 1代表过期
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * vip开通时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * vip开通时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 开通月数
     */
    public Integer getMonth() {
        return month;
    }

    /**
     * 开通月数
     */
    public void setMonth(Integer month) {
        this.month = month;
    }

    public Vip() {
    }

    public Vip(Integer id, Integer userId, Integer status, Date createTime, Integer month) {
        this.id = id;
        this.userId = userId;
        this.status = status;
        this.createTime = createTime;
        this.month = month;
    }

    public Vip(Integer id, Integer userId, Integer month) {
        this.id = id;
        this.userId = userId;
        this.month = month;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Vip other = (Vip) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getMonth() == null ? other.getMonth() == null : this.getMonth().equals(other.getMonth()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getMonth() == null) ? 0 : getMonth().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", status=").append(status);
        sb.append(", createTime=").append(createTime);
        sb.append(", month=").append(month);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}