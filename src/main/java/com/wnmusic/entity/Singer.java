package com.wnmusic.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 歌手
 * @TableName singer
 */
public class Singer implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 性别（男、女、组合）
     */
    private String gender;

    /**
     * 头像
     */
    private String pic;

    /**
     * 生日
     */
    private Date birth;

    /**
     * 地区
     */
    private String location;

    /**
     * 简介
     */
    private String introduction;

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 性别（男、女、组合）
     */
    public String getGender() {
        return gender;
    }

    /**
     * 性别（男、女、组合）
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * 头像
     */
    public String getPic() {
        return pic;
    }

    /**
     * 头像
     */
    public void setPic(String pic) {
        this.pic = pic;
    }

    /**
     * 生日
     */
    public Date getBirth() {
        return birth;
    }

    /**
     * 生日
     */
    public void setBirth(Date birth) {
        this.birth = birth;
    }

    /**
     * 地区
     */
    public String getLocation() {
        return location;
    }

    /**
     * 地区
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * 简介
     */
    public String getIntroduction() {
        return introduction;
    }

    /**
     * 简介
     */
    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Singer other = (Singer) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getGender() == null ? other.getGender() == null : this.getGender().equals(other.getGender()))
            && (this.getPic() == null ? other.getPic() == null : this.getPic().equals(other.getPic()))
            && (this.getBirth() == null ? other.getBirth() == null : this.getBirth().equals(other.getBirth()))
            && (this.getLocation() == null ? other.getLocation() == null : this.getLocation().equals(other.getLocation()))
            && (this.getIntroduction() == null ? other.getIntroduction() == null : this.getIntroduction().equals(other.getIntroduction()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getGender() == null) ? 0 : getGender().hashCode());
        result = prime * result + ((getPic() == null) ? 0 : getPic().hashCode());
        result = prime * result + ((getBirth() == null) ? 0 : getBirth().hashCode());
        result = prime * result + ((getLocation() == null) ? 0 : getLocation().hashCode());
        result = prime * result + ((getIntroduction() == null) ? 0 : getIntroduction().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", gender=").append(gender);
        sb.append(", pic=").append(pic);
        sb.append(", birth=").append(birth);
        sb.append(", location=").append(location);
        sb.append(", introduction=").append(introduction);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}