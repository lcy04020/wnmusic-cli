package com.wnmusic.entity;

import java.io.Serializable;

/**
 * 
 * @TableName song_album
 */
public class SongAlbum implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 专辑标题
     */
    private String title;

    /**
     * 包含歌曲数量
     */
    private Integer songCount;

    /**
     * 专辑封面
     */
    private String pic;

    /**
     * 专辑介绍
     */
    private String introduce;

    /**
     * 专辑风格
     */
    private String style;

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 专辑标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 专辑标题
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 包含歌曲数量
     */
    public Integer getSongCount() {
        return songCount;
    }

    /**
     * 包含歌曲数量
     */
    public void setSongCount(Integer songCount) {
        this.songCount = songCount;
    }

    /**
     * 专辑封面
     */
    public String getPic() {
        return pic;
    }

    /**
     * 专辑封面
     */
    public void setPic(String pic) {
        this.pic = pic;
    }

    /**
     * 专辑介绍
     */
    public String getIntroduce() {
        return introduce;
    }

    /**
     * 专辑介绍
     */
    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    /**
     * 专辑风格
     */
    public String getStyle() {
        return style;
    }

    /**
     * 专辑风格
     */
    public void setStyle(String style) {
        this.style = style;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SongAlbum other = (SongAlbum) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getTitle() == null ? other.getTitle() == null : this.getTitle().equals(other.getTitle()))
            && (this.getSongCount() == null ? other.getSongCount() == null : this.getSongCount().equals(other.getSongCount()))
            && (this.getPic() == null ? other.getPic() == null : this.getPic().equals(other.getPic()))
            && (this.getIntroduce() == null ? other.getIntroduce() == null : this.getIntroduce().equals(other.getIntroduce()))
            && (this.getStyle() == null ? other.getStyle() == null : this.getStyle().equals(other.getStyle()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getTitle() == null) ? 0 : getTitle().hashCode());
        result = prime * result + ((getSongCount() == null) ? 0 : getSongCount().hashCode());
        result = prime * result + ((getPic() == null) ? 0 : getPic().hashCode());
        result = prime * result + ((getIntroduce() == null) ? 0 : getIntroduce().hashCode());
        result = prime * result + ((getStyle() == null) ? 0 : getStyle().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", title=").append(title);
        sb.append(", songCount=").append(songCount);
        sb.append(", pic=").append(pic);
        sb.append(", introduce=").append(introduce);
        sb.append(", style=").append(style);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}