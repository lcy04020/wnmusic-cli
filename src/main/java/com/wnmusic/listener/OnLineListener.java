package com.wnmusic.listener;

import com.wnmusic.entity.SongList;
import com.wnmusic.service.SongListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.List;

@Component
public class OnLineListener implements HttpSessionListener {

    SongListService songListService;

    @Autowired
    public void setSongListService(SongListService songListService) {
        this.songListService = songListService;
    }

    @Override
    public void sessionCreated(HttpSessionEvent event) {
        System.out.println("---有用户上线了---");
        //当我们创建了一个session对象我们就认为由用户上线了
        System.out.println("展示书籍初始化");
        HttpSession session = event.getSession();
//        List<SongList> all = songListService.findAll();
//        session.setAttribute("songList",all);
        ServletContext sc = event.getSession().getServletContext();
        Object online = sc.getAttribute("online");
        if(online == null){
            //第一个人上线
            sc.setAttribute("online",1);
        }else{
            System.out.println("第二次");
            System.out.println(Long.parseLong(online.toString())+1);
            sc.setAttribute("online",Long.parseLong(online.toString())+1);
        }
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        System.out.println("---有用户下线了---");
        //当销毁了一个session对象我们就认为由用户下线了
        ServletContext context = se.getSession().getServletContext();
        Object online = context.getAttribute("online");
        if(online == null){
            //第一个人上线
            context.setAttribute("online",0);
        }else{
            context.setAttribute("online",Long.parseLong(online.toString())-1);
        }
    }
}
