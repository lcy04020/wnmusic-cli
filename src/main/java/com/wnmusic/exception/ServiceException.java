package com.wnmusic.exception;


/**
 * 自定义异常
 *  自定义异常的流程：两步骤
 *      1.继承类，一般继承的都是RuntimeException
 *      2.提供2个构造器 一个无参，一个有参：String -》 错误描述 message
 */
public class ServiceException extends RuntimeException {
    private Integer code;

    public Integer getCode() {
        return code;
    }

    public ServiceException(Integer code, String msg) {
        super(msg);
        this.code = code;
    }

}