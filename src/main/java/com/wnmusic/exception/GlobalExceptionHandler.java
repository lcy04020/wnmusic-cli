package com.wnmusic.exception;

import com.wnmusic.common.Result;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 如果抛出的的是ServiceException，则调用该方法
     *
     * 如果捕获到异常，需要把异常信息封装成Result 返回给前台
     *
     * @param se 业务异常
     * @return Result
     */
    @ExceptionHandler(ServiceException.class)
    @ResponseBody
    public Result handle(ServiceException se, HttpServletResponse response) throws IOException {
        response.getWriter().write("<script>alert('未登录，请重新登录')</script>");
        return Result.resultBuilder().result(false).state(se.getCode()).description(se.getMessage());
    }
}