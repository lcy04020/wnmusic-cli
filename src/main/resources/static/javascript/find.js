﻿function findSong(){
    $(".find-box a").removeAttr("style");
    $(".a1").css("color","#31c27c");
    $(".songList").remove();
    $(".songAlbum").remove();
    $(".song").show();

}

function findSongList(){
    $(".find-box a").removeAttr("style");
    $(".a2").css("color","#31c27c");
    $.ajax({
        type:"post",
        data:{
            songName:name
        },
        url:'../songList/fuzzyFind',
        success:function(result){
            let songList = result.data.list;
            $(".song").hide();
            $(".songLiat").remove();
            $(".songAlbum").remove();
            $("table").append(`<tr class="songList"><td>歌单</td></tr>`)
            for(let i = 0;i<songList.length;i++){
                console.log(songList[i].title)
                $(".table tbody").append(
                    `
                        <tr class="songList" style="height: 70px">
                            <td>
                                <img style="width: 50px" src="${songList[i].pic}">
                                <a onclick="toOneSongList(${songList[i].id})">${songList[i].title}</a>
                            </td>
                        </tr>
                    `
                )
            }
        }
    })
}

function findSongAlbum(){
    $(".find-box a").removeAttr("style");
    $(".a3").css("color","#31c27c");
    $.ajax({
        type:"post",
        data:{
            songName:name
        },
        url:'../songAlbum/fuzzyFind',
        success:function(result){
            let songAlbum = result.data.list;
            $(".song").hide();
            $(".songList").remove();
            $(".songAlbum").remove();
            $("table").append(`<tr class="songAlbum"><td>专辑</td><td>歌手</td></tr>`)
            for(let i = 0;i<songAlbum.length;i++){
                console.log(songAlbum[i].title)
                $(".table tbody").append(
                    `
                        <tr class="songAlbum" style="height: 70px">
                            <td>
                                <img style="width: 50px;height: 50px" src="${songAlbum[i].pic}">
                                <a onclick="toOneSongAlbum(${songAlbum[i].albumId})">${songAlbum[i].title}</a>
                            </td>
                            <td>
                                <a onclick="toSinger(${songAlbum[i].singerName})">${songAlbum[i].singerName}</a>
                            </td>
                        </tr>
                    `
                )
            }
        }
    })
}