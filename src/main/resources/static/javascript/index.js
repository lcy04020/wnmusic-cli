function loadAll() {
    loadSongList()
    loadSong()
    loadSongAlbum()
    loadSongRank()
    loadSongListRank()
    loadSongRankAndTime()
    loadSongListRankByStyle()
    loadSongAlbumRank()
    checkTradeStatus()
}

function checkTradeStatus(){
    if($("#tradeStatus").html() == "TRADE_SUCCESS"){
        console.log("价格"+$("#money").html())
        console.log("开通成功")
        $.ajax({
            type: "POST",
            url: "/wnmusic/vip/openVip",
            data: {
                "Authorization": localStorage.getItem("Authorization"),
                "money":$("#money").html(),
            },
            success: function (resp) {
                $("#tradeStatus").html("")
                $("#money").html("")
                alert(resp.description);

                // 检查是不是vip
                $.ajax({
                    type: "POST",
                    url: "/wnmusic/vip/checkVip",
                    data: {
                        "Authorization": localStorage.getItem("Authorization")
                    },
                    success: function (resp) {
                        localStorage.setItem("userVip", resp.data.userVipStatus)
                        checkLogin()
                        location.href="/wnmusic"
                    }
                })
            }
        })
    }
}

function loadSongList() {
    $(".sl0,.sl5,.sl10").children().remove()
    $.ajax({
        type: "get",
        url: "/wnmusic/songList/randomFind",
        success: function (result) {
            let list = result.data.songList
            //setTimeout(function () {
                for (var i = 0; i < list.length; i += 5) {
                    $(`.sl${i}`).append(`
                        <ul>
                            <li onclick="toOneSongList(${list[i].id})">
                                <img src="${list[i].pic}" style="width:224px">
                                <p style="width:224px">${list[i].title}</p>
                            </li>
                            <li onclick="toOneSongList(${list[i+1].id})">
                                <img src=" ${list[i + 1].pic}" style="width:224px">
                                <p style="width:224px">${list[i + 1].title}</p>
                            </li>
                            <li onclick="toOneSongList(${list[i+2].id})">
                                <img src=" ${list[i + 2].pic}" style="width:224px">
                                <p style="width:224px">${list[i + 2].title}</p>
                            </li>
                            <li onclick="toOneSongList(${list[i+3].id})">
                                <img src=" ${list[i + 3].pic}" style="width:224px">
                                <p style="width:224px">${list[i + 3].title}</p>
                            </li>
                            <li onclick="toOneSongList(${list[i+4].id})">
                                <img src=" ${list[i + 4].pic}" style="width:224px">
                                <p style="width:224px">${list[i + 4].title}</p>
                            </li>
                        </ul>
                        `)
                }
            //}, 100);
        }
    })
}

function loadSong() {
    $(".s0,.s15,.s30").children().remove()
    $.ajax({
        type: "get",
        url: "/wnmusic/song/randomFind",
        success: function (result) {
            let list = result.data.list
            setTimeout(function () {
                for (var i = 0; i < list.length; i += 15) {
                    $(`.s${i}`).append(`
                        <ul>
                            <li onclick="toOneSong(${list[i].id})">
                                <img src="${list[i].pic}">
                                <p style="width:224px">${list[i].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+1].id})">
                                <img src=" ${list[i + 1].pic}">
                                <p style="width:224px">${list[i + 1].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+2].id})">
                                <img src=" ${list[i + 2].pic}">
                                <p style="width:224px">${list[i + 2].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+3].id})">
                                <img src=" ${list[i + 3].pic}">
                                <p style="width:224px">${list[i + 3].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+4].id})">
                                <img src=" ${list[i + 4].pic}">
                                <p style="width:224px">${list[i + 4].name}</p>
                            </li>
                        </ul>
                        <ul>
                            <li onclick="toOneSong(${list[i+5].id})">
                                <img src="${list[i + 5].pic}">
                                <p style="width:224px">${list[i + 5].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+6].id})">
                                <img src=" ${list[i + 6].pic}">
                                <p style="width:224px">${list[i + 6].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[+7].id})">
                                <img src=" ${list[i + 7].pic}">
                                <p style="width:224px">${list[i + 7].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+8].id})">
                                <img src=" ${list[i + 8].pic}">
                                <p style="width:224px">${list[i + 8].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+9].id})">
                                <img src=" ${list[i + 9].pic}">
                                <p style="width:224px">${list[i + 9].name}</p>
                            </li>
                        </ul>
                        <ul>
                            <li onclick="toOneSong(${list[i+10].id})">
                                <img src="${list[i + 10].pic}">
                                <p style="width:224px">${list[i + 10].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+11].id})">
                                <img src=" ${list[i + 11].pic}">
                                <p style="width:224px">${list[i + 11].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+12].id})">
                                <img src=" ${list[i + 12].pic}">
                                <p style="width:224px">${list[i + 12].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+13].id})">
                                <img src=" ${list[i + 13].pic}">
                                <p style="width:224px">${list[i + 13].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+14].id})">
                                <img src=" ${list[i + 14].pic}">
                                <p style="width:224px">${list[i + 14].name}</p>
                            </li>
                        </ul>
                        `)
                }
            }, 1000);
        }
    })

}

function loadNewSong() {
    $(".s0,.s15,.s30").children().remove()
    $.ajax({
        type: "get",
        url: "/wnmusic/song/findFifteen",
        success: function (result) {
            let list = result.data.list
            setTimeout(function () {
                for (var i = 0; i < list.length; i += 15) {
                    $(`.s${i}`).append(`
                        <ul>
                            <li onclick="toOneSong(${list[i].id})">
                                <img src="${list[i].pic}">
                                <p style="width:224px">${list[i].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+1].id})">
                                <img src=" ${list[i + 1].pic}">
                                <p style="width:224px">${list[i + 1].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+2].id})">
                                <img src=" ${list[i + 2].pic}">
                                <p style="width:224px">${list[i + 2].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+3].id})">
                                <img src=" ${list[i + 3].pic}">
                                <p style="width:224px">${list[i + 3].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+4].id})">
                                <img src=" ${list[i + 4].pic}">
                                <p style="width:224px">${list[i + 4].name}</p>
                            </li>
                        </ul>
                        <ul>
                            <li onclick="toOneSong(${list[i+5].id})">
                                <img src="${list[i + 5].pic}">
                                <p style="width:224px">${list[i + 5].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+6].id})">
                                <img src=" ${list[i + 6].pic}">
                                <p style="width:224px">${list[i + 6].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[+7].id})">
                                <img src=" ${list[i + 7].pic}">
                                <p style="width:224px">${list[i + 7].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+8].id})">
                                <img src=" ${list[i + 8].pic}">
                                <p style="width:224px">${list[i + 8].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+9].id})">
                                <img src=" ${list[i + 9].pic}">
                                <p style="width:224px">${list[i + 9].name}</p>
                            </li>
                        </ul>
                        <ul>
                            <li onclick="toOneSong(${list[i+10].id})">
                                <img src="${list[i + 10].pic}">
                                <p style="width:224px">${list[i + 10].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+11].id})">
                                <img src=" ${list[i + 11].pic}">
                                <p style="width:224px">${list[i + 11].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+12].id})">
                                <img src=" ${list[i + 12].pic}">
                                <p style="width:224px">${list[i + 12].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+13].id})">
                                <img src=" ${list[i + 13].pic}">
                                <p style="width:224px">${list[i + 13].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+14].id})">
                                <img src=" ${list[i + 14].pic}">
                                <p style="width:224px">${list[i + 14].name}</p>
                            </li>
                        </ul>
                        `)
                }
            }, 200);
        }
    })
}

function loadSongByRank() {
    $(".s0,.s15,.s30").children().remove()
    $.ajax({
        type: "get",
        url: "/wnmusic/song/findFifteenByRank",
        success: function (result) {
            let list = result.data.list
            setTimeout(function () {
                for (var i = 0; i < list.length; i += 15) {
                    $(`.s${i}`).append(`
                        <ul>
                            <li onclick="toOneSong(${list[i].id})">
                                <img src="${list[i].pic}">
                                <p style="width:224px">${list[i].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+1].id})">
                                <img src=" ${list[i + 1].pic}">
                                <p style="width:224px">${list[i + 1].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+2].id})">
                                <img src=" ${list[i + 2].pic}">
                                <p style="width:224px">${list[i + 2].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+3].id})">
                                <img src=" ${list[i + 3].pic}">
                                <p style="width:224px">${list[i + 3].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+4].id})">
                                <img src=" ${list[i + 4].pic}">
                                <p style="width:224px">${list[i + 4].name}</p>
                            </li>
                        </ul>
                        <ul>
                            <li onclick="toOneSong(${list[i+5].id})">
                                <img src="${list[i + 5].pic}">
                                <p style="width:224px">${list[i + 5].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+6].id})">
                                <img src=" ${list[i + 6].pic}">
                                <p style="width:224px">${list[i + 6].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[+7].id})">
                                <img src=" ${list[i + 7].pic}">
                                <p style="width:224px">${list[i + 7].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+8].id})">
                                <img src=" ${list[i + 8].pic}">
                                <p style="width:224px">${list[i + 8].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+9].id})">
                                <img src=" ${list[i + 9].pic}">
                                <p style="width:224px">${list[i + 9].name}</p>
                            </li>
                        </ul>
                        <ul>
                            <li onclick="toOneSong(${list[i+10].id})">
                                <img src="${list[i + 10].pic}">
                                <p style="width:224px">${list[i + 10].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+11].id})">
                                <img src=" ${list[i + 11].pic}">
                                <p style="width:224px">${list[i + 11].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+12].id})">
                                <img src=" ${list[i + 12].pic}">
                                <p style="width:224px">${list[i + 12].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+13].id})">
                                <img src=" ${list[i + 13].pic}">
                                <p style="width:224px">${list[i + 13].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+14].id})">
                                <img src=" ${list[i + 14].pic}">
                                <p style="width:224px">${list[i + 14].name}</p>
                            </li>
                        </ul>
                        `)
                }
            }, 200);
        }
    })
}

function loadOldSong() {
    $(".s0,.s15,.s30").children().remove()
    $.ajax({
        type: "get",
        url: "/wnmusic/song/findOldFifteen",
        success: function (result) {
            let list = result.data.list
            setTimeout(function () {
                for (var i = 0; i < list.length; i += 15) {
                    $(`.s${i}`).append(`
                        <ul>
                            <li onclick="toOneSong(${list[i].id})">
                                <img src="${list[i].pic}">
                                <p style="width:224px">${list[i].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+1].id})">
                                <img src=" ${list[i + 1].pic}">
                                <p style="width:224px">${list[i + 1].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+2].id})">
                                <img src=" ${list[i + 2].pic}">
                                <p style="width:224px">${list[i + 2].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+3].id})">
                                <img src=" ${list[i + 3].pic}">
                                <p style="width:224px">${list[i + 3].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+4].id})">
                                <img src=" ${list[i + 4].pic}">
                                <p style="width:224px">${list[i + 4].name}</p>
                            </li>
                        </ul>
                        <ul>
                            <li onclick="toOneSong(${list[i+5].id})">
                                <img src="${list[i + 5].pic}">
                                <p style="width:224px">${list[i + 5].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+6].id})">
                                <img src=" ${list[i + 6].pic}">
                                <p style="width:224px">${list[i + 6].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[+7].id})">
                                <img src=" ${list[i + 7].pic}">
                                <p style="width:224px">${list[i + 7].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+8].id})">
                                <img src=" ${list[i + 8].pic}">
                                <p style="width:224px">${list[i + 8].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+9].id})">
                                <img src=" ${list[i + 9].pic}">
                                <p style="width:224px">${list[i + 9].name}</p>
                            </li>
                        </ul>
                        <ul>
                            <li onclick="toOneSong(${list[i+10].id})">
                                <img src="${list[i + 10].pic}">
                                <p style="width:224px">${list[i + 10].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+11].id})">
                                <img src=" ${list[i + 11].pic}">
                                <p style="width:224px">${list[i + 11].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+12].id})">
                                <img src=" ${list[i + 12].pic}">
                                <p style="width:224px">${list[i + 12].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+13].id})">
                                <img src=" ${list[i + 13].pic}">
                                <p style="width:224px">${list[i + 13].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+14].id})">
                                <img src=" ${list[i + 14].pic}">
                                <p style="width:224px">${list[i + 14].name}</p>
                            </li>
                        </ul>
                        `)
                }
            }, 200);
        }
    })
}

function loadClaSong() {
    $(".s0,.s15,.s30").children().remove()
    $.ajax({
        type: "get",
        url: "/wnmusic/song/findClaFifteen",
        success: function (result) {
            let list = result.data.list
            setTimeout(function () {
                for (var i = 0; i < list.length; i += 15) {
                    $(`.s${i}`).append(`
                        <ul>
                            <li onclick="toOneSong(${list[i].id})">
                                <img src="${list[i].pic}">
                                <p style="width:224px">${list[i].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+1].id})">
                                <img src=" ${list[i + 1].pic}">
                                <p style="width:224px">${list[i + 1].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+2].id})">
                                <img src=" ${list[i + 2].pic}">
                                <p style="width:224px">${list[i + 2].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+3].id})">
                                <img src=" ${list[i + 3].pic}">
                                <p style="width:224px">${list[i + 3].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+4].id})">
                                <img src=" ${list[i + 4].pic}">
                                <p style="width:224px">${list[i + 4].name}</p>
                            </li>
                        </ul>
                        <ul>
                            <li onclick="toOneSong(${list[i+5].id})">
                                <img src="${list[i + 5].pic}">
                                <p style="width:224px">${list[i + 5].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+6].id})">
                                <img src=" ${list[i + 6].pic}">
                                <p style="width:224px">${list[i + 6].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[+7].id})">
                                <img src=" ${list[i + 7].pic}">
                                <p style="width:224px">${list[i + 7].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+8].id})">
                                <img src=" ${list[i + 8].pic}">
                                <p style="width:224px">${list[i + 8].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+9].id})">
                                <img src=" ${list[i + 9].pic}">
                                <p style="width:224px">${list[i + 9].name}</p>
                            </li>
                        </ul>
                        <ul>
                            <li onclick="toOneSong(${list[i+10].id})">
                                <img src="${list[i + 10].pic}">
                                <p style="width:224px">${list[i + 10].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+11].id})">
                                <img src=" ${list[i + 11].pic}">
                                <p style="width:224px">${list[i + 11].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+12].id})">
                                <img src=" ${list[i + 12].pic}">
                                <p style="width:224px">${list[i + 12].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+13].id})">
                                <img src=" ${list[i + 13].pic}">
                                <p style="width:224px">${list[i + 13].name}</p>
                            </li>
                            <li onclick="toOneSong(${list[i+14].id})">
                                <img src=" ${list[i + 14].pic}">
                                <p style="width:224px">${list[i + 14].name}</p>
                            </li>
                        </ul>
                        `)
                }
            }, 200);
        }
    })
}

function loadSongAlbum() {
    $(".sa0,.sa5,.sa10").children().remove()
    $.ajax({
        type: "get",
        url: "/wnmusic/songAlbum/randomFind",
        success: function (result) {
            let list = result.data.songAlbum
            setTimeout(function () {
                for (var i = 0; i < list.length; i += 5) {
                    $(`.sa${i}`).append(`
                        <ul>
                            <li onclick="toOneSongAlbum(${list[i].id})">
                                <img src="${list[i].pic}" style="width:224px">
                                <p style="width:224px">${list[i].title}</p>
                            </li>
                            <li onclick="toOneSongAlbum(${list[i+1].id})">
                                <img src=" ${list[i + 1].pic}" style="width:224px">
                                <p style="width:224px">${list[i + 1].title}</p>
                            </li>
                            <li onclick="toOneSongAlbum(${list[i+2].id})">
                                <img src=" ${list[i + 2].pic}" style="width:224px">
                                <p style="width:224px">${list[i + 2].title}</p>
                            </li>
                            <li onclick="toOneSongAlbum(${list[i+3].id})">
                                <img src=" ${list[i + 3].pic}" style="width:224px">
                                <p style="width:224px">${list[i + 3].title}</p>
                            </li>
                            <li onclick="toOneSongAlbum(${list[i+4].id})">
                                <img src=" ${list[i + 4].pic}" style="width:224px">
                                <p style="width:224px">${list[i + 4].title}</p>
                            </li>
                        </ul>
                        `)
                }
            }, 2000);
        }
    })
}

function loadSongAlbumByStyle(style) {
    $(".sa0,.sa5,.sa10").children().remove()
    $.ajax({
        type: "post",
        data:{
          style:style
        },
        url: "/wnmusic/songAlbum/findByStyle",
        success: function (result) {
            let list = result.data.songAlbum
            setTimeout(function () {
                for (let i = 0; i < list.length; i += 5) {
                    $(`.sa${i}`).append(`
                        <ul>
                            <li onclick="toOneSongAlbum(${list[i].id})">
                                <img src="${list[i].pic}" style="width:224px" alt="">
                                <p style="width:224px">${list[i].title}</p>
                            </li>
                            <li onclick="toOneSongAlbum(${list[i+1].id})">
                                <img src=" ${list[i + 1].pic}" style="width:224px">
                                <p style="width:224px">${list[i + 1].title}</p>
                            </li>
                            <li onclick="toOneSongAlbum(${list[i+2].id})">
                                <img src=" ${list[i + 2].pic}" style="width:224px">
                                <p style="width:224px">${list[i + 2].title}</p>
                            </li>
                            <li onclick="toOneSongAlbum(${list[i+3].id})">
                                <img src=" ${list[i + 3].pic}" style="width:224px">
                                <p style="width:224px">${list[i + 3].title}</p>
                            </li>
                            <li onclick="toOneSongAlbum(${list[i+4].id})">
                                <img src=" ${list[i + 4].pic}" style="width:224px">
                                <p style="width:224px">${list[i + 4].title}</p>
                            </li>
                        </ul>
                        `)
                }
            }, 300);
        }
    })
}

function loadSongRank(){
    $.ajax({
        type: "get",
        url: "/wnmusic/rank/songRank",
        success: function (result) {
            let list = result.data.songRank
            setTimeout(function () {
                for (var i = 0; i < 3; i++) {
                    $(`.b1`).append(`
                            <div class="rank-title3" onclick="toOneSong(${list[i].songId})">${i+1}　${list[i].name}</div>
                        `)
                }
            }, 3000);
        }
    })
}

function loadSongRankAndTime(){
    $.ajax({
        type: "get",
        url: "/wnmusic/rank/newSongRank",
        success: function (result) {
            let list = result.data.songRankAndTime
            setTimeout(function () {
                for (var i = 0; i < 3; i++) {
                    $(`.b2`).append(`
                            <div class="rank-title3" onclick="toOneSong(${list[i].songId})">${i+1}　${list[i].name}</div>
                        `)
                }
            }, 3000);
        }
    })
}

function loadSongListRank(){
    $.ajax({
        type: "get",
        url: "/wnmusic/rank/songListRank",
        success: function (result) {
            let list = result.data.songListRank
            setTimeout(function () {
                for (var i = 0; i < 3; i++) {
                    $(`.b3`).append(`
                            <div class="rank-title3" onclick="toOneSongList(${list[i].songListId})">${i+1}　${list[i].title}</div>
                        `)
                }
            }, 3000);
        }
    })
}

function loadSongListRankByStyle(){
    $.ajax({
        type: "get",
        data:{
            style:"华语"
        },
        url: "/wnmusic/rank/songListRankByStyle",
        success: function (result) {
            let list = result.data.songListRankByStyle
            setTimeout(function () {
                for (var i = 0; i < 3; i++) {
                    $(`.b4`).append(`
                            <div class="rank-title3" onclick="toOneSongList(${list[i].songListId})">${i+1}　${list[i].title}</div>
                        `)
                }
            }, 3000);
        }
    })
}

function loadSongAlbumRank(){
    $.ajax({
        type: "get",
        url: "/wnmusic/rank/songAlbumRank",
        success: function (result) {
            let list = result.data.songAlbumRank
            setTimeout(function () {
                for (var i = 0; i < 3; i++) {
                    $(`.b5`).append(`
                            <div class="rank-title3" onclick="toOneSongAlbum(${list[i].songAlbumId})">${i+1}　${list[i].title}</div>
                        `)
                }
            }, 3000);
        }
    })
}

function loadSongListByStyle(style) {
    $(".sl0,.sl5,.sl10").children().remove()
    $.ajax({
        type: "post",
        data:{
          style:style
        },
        url: "/wnmusic/songList/findByStyle",
        success: function (result) {
            let list = result.data.songList
            setTimeout(function () {
                for (var i = 0; i < list.length; i += 5) {
                    $(`.sl${i}`).append(`
                        <ul>
                            <li onclick="toOneSongList(${list[i].id})">
                                <img src="${list[i].pic}" style="width:224px">
                                <p style="width:224px">${list[i].title}</p>
                            </li>
                            <li onclick="toOneSongList(${list[i+1].id})">
                                <img src=" ${list[i + 1].pic}" style="width:224px">
                                <p style="width:224px">${list[i + 1].title}</p>
                            </li>
                            <li onclick="toOneSongList(${list[i+2].id})">
                                <img src=" ${list[i + 2].pic}" style="width:224px">
                                <p style="width:224px">${list[i + 2].title}</p>
                            </li>
                            <li onclick="toOneSongList(${list[i+3].id})">
                                <img src=" ${list[i + 3].pic}" style="width:224px">
                                <p style="width:224px">${list[i + 3].title}</p>
                            </li>
                            <li onclick="toOneSongList(${list[i+4].id})">
                                <img src=" ${list[i + 4].pic}" style="width:224px">
                                <p style="width:224px">${list[i + 4].title}</p>
                            </li>
                        </ul>
                        `)
                }
            }, 100);
        }
    })
}

function toOneSongList(id){
    console.log("songList",id)
    location.href = "/wnmusic/songList/findListByKey?id="+id;

}

function toOneSong(id){
    console.log("song",id)
    location.href = "/wnmusic/song/findSongDetail?id="+id;
}

function toOneSongAlbum(id){
    console.log("songAlbum",id)
    location.href = "/wnmusic/songAlbum/findAlbumByKey?id="+id;
}