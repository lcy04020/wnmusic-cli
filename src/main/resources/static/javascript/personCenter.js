function loadAll(userName){

    loadCollect(userName)
    loadheadInfo(userName)
    //loadUser(userName)

    /* 歌曲 */

    $(".mySong").click(function (){
        $(" table").remove();
        $(".song-info ul").remove();
        $(".songList-info ul").remove();
        loadCollect(userName)
    })



    /* 歌单 */
    $(".songList").click(function (){
        $(" table").remove();
        $(".song-info ul").remove();
        $(".songList-info ul").remove();


        $.ajax({
            url: "./findSongListByName",
            type: "get",
            data: {
                "userName" : userName
            },

            success:function (result) {
                let list = result.data.songList

                console.log("result.data.songList:"+list)

                setTimeout(function(){
                    for (var i = 0;i < list.length;i++) {
                        $(`  .songList-info`).append(`

                            <ul style="display: inline;overflow:hidden;">
                                <li onclick="toOneSongList(${list[i].id})" style="margin-left: 20px;margin-right: 30px;float: left">
                                    <img src="${list[i].songList.pic}" style="width:224px ;height: 224px;border-radius: 5%">
                                    <p style="width:224px">${list[i].songList.title}</p>
                                </li>
                            </ul>
                               
                            
                        `)

                        console.log(list[i].songAlbum.name)
                    }},100);

            }
        })

    })



    /* 专辑 */
    $(".songAlbum").click(function (){
        $(" table").remove();
        $(".song-info ul").remove();
        $(".songList-info ul").remove();

        $.ajax({
            url: "./findSongAlbumByName",
            type: "get",
            data: {
                "userName" : userName
            },

            success:function (result) {
                let list = result.data.songAlbum

                console.log("result.data.songAlbum:"+list)

                setTimeout(function(){
                    for (var i = 0;i < list.length;i++) {
                        $(`  .song-info`).append(`

                            <ul style="display: inline;overflow: hidden">
                                <li onclick="toOneSongList(${list[i].id})" style="margin-left: 20px;margin-right: 30px; float: left">
                                    <img src="${list[i].songAlbum.pic}" style="width:224px;height: 224px;border-radius: 5%">
                                    <p style="width:224px;text-align: center">${list[i].songAlbum.title}</p>
                                </li>
                            </ul>
                            
                        `)

                        console.log(list[i].songAlbum.name)
                    }},100);

            }
        })
    })



    /* 我喜欢 */
    $(".myLike").click(function (){
        location.reload();

        $.ajax({
            url: "./findAllCollect",
            type: "get",
            data: {
                "userName" : userName
            },

            success:function (result) {

                $(".type-page").remove()
                $(".personInfo").remove()

                let list = result.data.user
                console.log("result.data.collectList:"+list)

                setTimeout(function(){
                    $(".myLikeList").append(`
                        <div class="type-page">
                            <div class="bottom-menu" >
                                <ul class="greenli">
                                    <li ><a class="mySong" onclick="mySong()">歌曲</a></li>
                                    <li ><a class="songList" onclick="songList()">歌单</a></li>
                                    <li ><a class="songAlbum">专辑</a></li>
                                </ul>
                            </div>
                
                            
                            
                            <div class="song-info">
                                
                                
                                <table class="mytable ml" style="width: 800px; height: 50px;margin-top: 10px;margin-left: 35px">
                                    <tr>
                                        <td><h2 style="margin-bottom: 30px">我喜欢</h2></td>
                                    </tr>
                                    <tr style="font-size: 20px;solid-color: black;margin-bottom: 35px">
                                        <td><input type="checkbox" name="id" hidden></td>
                                        <td>歌曲</td>
                                        <td>封面</td>
                                    </tr>
                                </table>
                            </div>
                        </div> 
                        
                    
                    `)

                    for (var i = 0;i < list.length;i++) {
                        console.log(list[i].song.name)
                        console.log(list[i].songAlbum.name)

                        $(`  table`).append(`
                            <tr>
                                <td>
                                    <input type="checkbox" hidden />
                                    ${i+1}
                                </td>
            
                                <td>
                                    ${list[i].song.name}
                                </td>
            
                                <td>
                                    <img src="${list[i].song.pic}" style="height: 30px;width: 30px;border-radius: 50%;"/>
                                </td>
            
            
                            </tr>
                            
                        `)
                    }},100);

            }
        })

    })




    /* 个人资料 */
    $(".ainfo").click(function (){
        $.ajax({
            url: "./findUser",
            type: "get",
            data: {
                "userName" : userName
            },
            success: function (result) {
                let user = result.data.user;
                let avatar = user.avatar;
                console.log(result)
                $(".type-page").remove()
                $(".personInfo").remove()
                //$("#heading").attr("src",avatar);
                $("#id").val(user.id);
                $("#userName").val(user.username)
                $("#phoneNum").val(user.phoneNum)
                $("#gender").val(user.gender)
                $("#birth").val(user.birth)
                $("#location").val(user.location)

                console.log("生日:"+user.birth)
                console.log("图片地址:"+avatar)
                $(`.user-info`).append(`
                    <div class="personInfo" style="margin-left: 290px;">
                       <h2 style="margin-bottom: 10px">个人资料</h2>

                        <div >昵称:<span>${user.username}</span></div>
                        <div >手机:<span>${user.phoneNum}</span></div>
                        <div >性别:<span>${user.gender}</span></div>
                        <div >生日:<span>${user.birth}</span></div>
                        <div >地区:<span>${user.location}</span></div>
                        <button data-toggle="modal" data-target="#updateUserModal" class="modify-a"><a>修改</a></button>
                    </div>
                `)

            }
        })
    })



}



/* 收藏 */
function loadCollect(userName) {
    console.log("loadCollect")
    $.ajax({
        url: "./findAllCollect",
        type: "get",
        data: {
            "userName" : userName
        },

        success:function (result) {
            let list = result.data.user
            console.log("result.data.collectList:"+list)

            $(".song-info").append(`
                <table class="mytable ml" style="width: 800px; height: 50px;margin-top: 10px;margin-left: 35px">
                    <tr>
                        <td><h2 style="margin-bottom: 10px">我喜欢</h2></td>
                    </tr>
                 
                    <tr style="font-size: 20px;solid-color: black;margin-bottom: 30px">
                        <td><input type="checkbox" name="id" hidden></td>
                        <td>歌曲</td>
                        <td>封面</td>
                    </tr>
                </table>
                  
            `)

            setTimeout(function(){

                for (var i = 0;i < list.length;i++) {
                    console.log("歌曲名:"+list[i].song.name)
                    console.log("歌曲图:"+list[i].song.pic)


                    $(`  table`).append(`
                        <tr>
                            <td>
                                <input type="checkbox" hidden />
                                ${i+1}
                            </td>
        
                            <td>
                                ${list[i].song.name}
                            </td>
        
                            
                            <td>
                                <img src="${list[i].song.pic}" style="height: 30px;width: 30px;border-radius: 50%"/>
                            </td>

                            
                           
        
                        </tr>
                            
                        `)
                }},100);

        }
    })
}


function loadheadInfo(userName) {
    $.ajax({
        type: "get",
        url: "./findUser",
        data: {
            "userName" : userName
        },
        success: function (result) {
            let user = result.data.user;
            let avatar = user.avatar;
            //$(".ainfo").style.fontcolor="#31C27C"


            $("#heading").attr("src",avatar);

        }
    })
}


/*个人资料*/
function loadUser(userName) {
    $.ajax({
        type: "get",
        url: "./findUser",
        data: {
            "userName" : userName
        },
        success: function (result) {
            let user = result.data.user;
            let avatar = user.avatar;
            //$(".ainfo").style.color="#31C27C"


            $("#heading").attr("src",avatar);
            $("#id").val(user.id);
            $("#userName").val(user.username)
            $("#phoneNum").val(user.phoneNum)
            $("#gender").val(user.gender)
            $("#birth").val(user.birth)
            $("#location").val(user.location)

            console.log("生日:"+user.birth)

            console.log("图片地址:"+avatar)
            $(` .user-info`).append(`
                    <h2 >个人资料</h2>
                    
                    <div >昵称:${user.username}</div> 
                    <div >手机:${user.phoneNum}</div>
                    <div >性别:${user.gender}</div>
                    <div >生日:${user.birth}</div>
                    <div >地区:${user.location}</div>
                    <button data-toggle="modal" data-target="#updateUserModal"><a class="modify-a">修改</a></button>
                
            `)

            /*<div >头像:<img src="${user.avatar}" style="height: 80px;width: 80px;border-radius: 50%"/></div>*/

        }
    })
}

