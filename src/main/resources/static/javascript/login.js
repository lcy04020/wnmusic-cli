function breakLogin() {
    localStorage.removeItem("userName")
    localStorage.removeItem("Authorization")
    localStorage.removeItem("userAvatar")
    localStorage.removeItem("userVip")
    checkLogin()
}


function userLogin() {

    $.ajax({
        type: "POST",
        url: "/wnmusic/user/login",
        data: $("#login").serialize(),
        success: function (response) {
            console.log(response)
            alert(response.description)

            if (response.result) {
                localStorage.setItem("userName", response.data.userName)
                localStorage.setItem("Authorization", response.data.Authorization)
                localStorage.setItem("userAvatar", response.data.user.avatar)


                //-- 判断是不是vip
                $.ajax({
                    type: "POST",
                    url: "/wnmusic/vip/checkVip",
                    data: {
                        "Authorization": localStorage.getItem("Authorization")
                    },
                    success: function (resp) {
                        localStorage.setItem("userVip", resp.data.userVipStatus)

                        checkLogin()
                        $("#myModal").css("display", "none")
                        $("div[class='modal-backdrop fade in']")[0].remove()
                        $("#username").val("");
                        $("#password").val("");
                    }
                })
            }
        }
    })

}

// var t;
//
// function openMess() {
//     clearInterval(t)
//     // 这里是延迟3秒执行程序---
//     t = setTimeout(function () {
//         $("#detail").attr("hidden", true)
//         console.log("testClose--------------")
//     }, 3000);
//     $("#detail").attr("hidden", false)
//     console.log("testOpen")
// }


// 注册的验证
$(function () {
    $('#register').bootstrapValidator({
        message: '这个值无效',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                message: '用户名无效',
                validators: {
                    notEmpty: {
                        message: '用户名不能为空'
                    },
                    stringLength: {
                        min: 2,
                        max: 10,
                        message: '用户名长度为2-10位'
                    },
                    remote: {
                        url: './user/checkName',
                        message: '名字已经被使用',
                        type: "GET",
                        delay: 500
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: '密码不能为空'
                    },
                    stringLength: {
                        min: 2,
                        max: 10,
                        message: '密码应该是2-10位的字符'
                    },
                }

            },
            password2: {
                validators: {
                    notEmpty: {
                        message: '密码必须填写'
                    },
                    identical: {
                        field: 'password',
                        message: '两次密码不同'
                    }
                }
            },
        }
    })
        .on('success.form.bv', function (e) {
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            // var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function (response) {

                console.log("注册用户后，得到的消息：" + response.result);
                if (response.result) {
                    alert("--- 注册成功! ---");
                    $("#myModal2").css("display", "none")
                    $("div[class='modal-backdrop fade in']")[1].remove()
                } else {
                    alert("--- 对不起，注册失败 ---");
                }

            }, 'json');
        });
});


