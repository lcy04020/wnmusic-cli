let city = '全部';
let gender = '全部';


$(function () {

    $(".singer_tag_item").click(function () {
        $(this).addClass("singer_tag_item_select")
        $(this).siblings("a").removeClass("singer_tag_item_select")
    });


    $(".city").click(function () {
        city = this.text
        $(".singer_list_list").empty()
        $(".singer_list_txt").empty()
        $.ajax({
            type: 'GET',
            url: './singer/findSingerByCondition?singerAddress=' + city + "&singerGender=" + gender,
            success: function (result) {
                let list = result.data.singerList;
                console.log(list)
                for (let i = 0; i < list.length; i++) {
                    if(i<10){
                        $(".singer_list_list").append(`

            <li class="singer_list_item">
                <div class="singer_list_item_box">
                    <a class="singer_list_cover" title="${list[i].name}" onclick="intoSingerDetail(${list[i].id})">
                        <img class="singer_list_pic" loading="lazy"
                             src="${list[i].pic}">
                    </a>
                    <h3 class="singer_list_title" style="min-height: 20px">
                        <a onclick="intoSingerDetail(${list[i].id})">${list[i].name}</a>
                    </h3>
                </div>
            </li>
                    
                    `)
                    }else{
                        $(".singer_list_txt").append(`
                    
        <li class="singer_list_txt_item">
            <a class="singer_list_txt_link js_singer" title="${list[i].name}" onclick="intoSingerDetail(${list[i].id})">
                ${list[i].name}
            </a>
        </li>
                    
                    `)
                    }
                }
            }
        })
    })

    $(".gender").click(function () {
        gender = this.text
        $(".singer_list_list").empty()
        $(".singer_list_txt").empty()
        $.ajax({
            type: 'GET',
            url: './singer/findSingerByCondition?singerAddress=' + city + "&singerGender=" + gender,
            success: function (result) {
                let list = result.data.singerList;
                console.log(list)
                for (let i = 0; i < list.length; i++) {
                    if(i<10){
                        $(".singer_list_list").append(`

            <li class="singer_list_item">
                <div class="singer_list_item_box">
                    <a class="singer_list_cover" title="${list[i].name}" onclick="intoSingerDetail(${list[i].id})">
                        <img class="singer_list_pic" loading="lazy"
                             src="${list[i].pic}">
                    </a>
                    <h3 class="singer_list_title" style="min-height: 20px">
                        <a onclick="intoSingerDetail(${list[i].id})">${list[i].name}</a>
                    </h3>
                </div>
            </li>
                    
                    `)
                    }else{
                        $(".singer_list_txt").append(`
                    
        <li class="singer_list_txt_item">
            <a class="singer_list_txt_link js_singer" title="${list[i].name}" onclick="intoSingerDetail(${list[i].id})">
                ${list[i].name}
            </a>
        </li>
                    
                    `)
                    }
                }
            }
        })
    })
});

function intoSingerDetail(id){
    location.href = "./singerDetail/findSingerById/"+id;
}

function loadData() {
    $.ajax({
        type: 'GET',
        url: './singer/findSingerByCondition?singerAddress=' + city + "&singerGender=" + gender,
        success: function (result) {
            let list = result.data.singerList;
            console.log(list)
            for (let i = 0; i < list.length; i++) {
                if(i<10){
                    $(".singer_list_list").append(`

            <li class="singer_list_item">
                <div class="singer_list_item_box">
                    <a class="singer_list_cover" title="${list[i].name}" onclick="intoSingerDetail(${list[i].id})">
                        <img class="singer_list_pic" loading="lazy"
                             src="${list[i].pic}">
                    </a>
                    <h3 class="singer_list_title" style="min-height: 20px">
                        <a onclick="intoSingerDetail(${list[i].id})">${list[i].name}</a>
                    </h3>
                </div>
            </li>
                    
                    `)
                }else{
                    $(".singer_list_txt").append(`
                    
        <li class="singer_list_txt_item">
            <a class="singer_list_txt_link js_singer" title="${list[i].name}" onclick="intoSingerDetail(${list[i].id})">
                ${list[i].name}
            </a>
        </li>
                    
                    `)
                }
            }
        }
    })
}