package com.wnmusic;

import com.alibaba.fastjson.JSON;
import com.wnmusic.entity.Song;
import com.wnmusic.entity.SongAlbum;
import com.wnmusic.entity.SongList;
import com.wnmusic.mapper.SongListMapper;
import com.wnmusic.mapper.SongMapper;
import com.wnmusic.queryvo.SongPlayerQueryVo;
import com.wnmusic.service.SongAlbumService;
import com.wnmusic.service.SongListService;
import com.wnmusic.service.SongService;
import com.wnmusic.utils.GetSongTime;
import com.wnmusic.utils.SplitLyric;
import org.apache.commons.collections4.Get;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;
import java.util.Objects;
import java.util.Set;

@SpringBootTest
class WnmusicCliApplicationTests {
    @Autowired
    private SongListService songListService;

    @Autowired
    private SongService songService;

    @Autowired
    private SongAlbumService songAlbumService;

    @Autowired
    StringRedisTemplate redisTemplate;

    //将歌曲信息存入redis
    @Test
    void contextLoads() {
        for (int i = 0;i<113;i++){
            SongPlayerQueryVo songPlayerById = songService.findSongPlayerById(i);
            if(!Objects.isNull(songPlayerById)){
                String lyric = songPlayerById.getLyric();
                String url = songPlayerById.getUrl();
                String[][] lyricArrays = SplitLyric.getLyricArrays(lyric);
                songPlayerById.setSongLyric(lyricArrays);
                songPlayerById.setLyric("");
                int songTime = GetSongTime.getSongTime(url);
                String s = GetSongTime.secToMin(songTime);
                songPlayerById.setSongTime(s);
                redisTemplate.opsForValue().set(i+"", JSON.toJSONString(songPlayerById));
            }
        }
    }

    @Test
    void contextLoad1() {
//        List<String> history = redisTemplate.opsForList().range("historySong", 0, -1);
//        System.out.println(history);
        Set<String> historyList = redisTemplate.opsForSet().members("historyList");
        System.out.println(historyList);
    }

    //将歌单信息存入redis
    @Test
    void contextLoad2() {
//        for (int i = 0; i < 85; i++) {
//            SongList byId = songListService.findById(i);
//            if(!Objects.isNull(byId)){
//                redisTemplate.opsForHash().put("allSongList",i+"",JSON.toJSONString(byId));
//            }
//        }
        String allSongList = redisTemplate.opsForHash().get("allSongList", "1").toString();
        SongList songList = JSON.parseObject(allSongList, SongList.class);
        System.out.println(songList);
    }

    //将专辑信息存入redis
    @Test
    void contextLoad3() {
        for (int i = 0; i < 111; i++) {
            SongAlbum byId = songAlbumService.findById(i);
            if(!Objects.isNull(byId)){
                redisTemplate.opsForHash().put("allSongAlbum",i+"",JSON.toJSONString(byId));
            }
        }
//        String allAlbum = redisTemplate.opsForHash().get("allSongAlbum", "1").toString();
//        SongAlbum songAlbum = JSON.parseObject(allAlbum, SongAlbum.class);
//        System.out.println(songAlbum);
    }

    //将歌曲信息存入redis
    @Test
    void contextLoad4() {
        for (int i = 0; i < 114; i++) {
            Song byId = songService.findById(i);
            if(!Objects.isNull(byId)){
                redisTemplate.opsForHash().put("allSong",i+"",JSON.toJSONString(byId));
            }
        }
//        String allAlbum = redisTemplate.opsForHash().get("allSongAlbum", "1").toString();
//        SongAlbum songAlbum = JSON.parseObject(allAlbum, SongAlbum.class);
//        System.out.println(songAlbum);
    }

    //将歌曲时长存入redis
    @Test
    void contextLoad5() {
        for (int i = 0; i < 114; i++) {
            Song byId = songService.findById(i);
            if(!Objects.isNull(byId)){
                String url = byId.getUrl();
                int songTime = GetSongTime.getSongTime(url);
                String s = GetSongTime.secToMin(songTime);
                redisTemplate.opsForHash().put("allSongTime",i+"",s);
            }
        }
//        String allAlbum = redisTemplate.opsForHash().get("allSongAlbum", "1").toString();
//        SongAlbum songAlbum = JSON.parseObject(allAlbum, SongAlbum.class);
//        System.out.println(songAlbum);
    }

}
